# er4hn.info

This is the repo for my personal website. It is based on hugo and deployed via Cloudflare Pages.

## Getting started

This branch contains submodules. Either clone it with `--recurse-submodules` or load the submodules afterwards with something like:

- `git clone <repo uri>`
- `cd <new folder>`
- `git submodule init`
- `git submodule update`

## Checking changes

Changes can be validated by displaying them with hugo. To do so:

- `hugo serve -D` from the root of the repo will serve up changes and display the host/port for them.
