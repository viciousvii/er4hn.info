---
title: "(Creativity ✏) Certificate of Good Standing"
description: "Then Life Continued - Creativity"
summary: In near-future cyberpunk Los Angeles, a young man tries to open a bakery
tags: ["Then Life Continued", "Creativity"]
date: 2023-01-14
---

## Intro

This was intended to be a short story. Due to an overabundance of enthusiasm and for lack of an editor it became far longer.

A key theme I tried to cover here was: What is cyberpunk in 2023?

## Chapter 1 - Real

"How many meat buns can I get for these sanitary pads?"

"What? None. What am I going to do with that? Do you have any liquor to trade?"

"Liquor? Why are you always asking for liquor Hugo? You decide you want to run a bar instead of a bakery?" Hector crossed his arms and scowled, the box of pads clung in his left hand as it crossed under his right arm.

As the two men haggled, commerce continued behind them. The streets by MacArthur Park were an open air market, filled with makeshift tents and stalls. Hugo was just one of the many vendors bartering, negotiating, and selling their products. Shampoo, razors, food, and clothes could all be found within steps of Hugo's own stall.

"No way Hector, it's been my dream to own a bakery. Alcohol is just better réal, easier to trade and all." The word réal rolled off the mans tongue with a curl on the e, French style. Hugo gave him a small smile. "Besides, what am I going to do with pads?"

"You live with your mom don't you? I'm sure she could use them." Hector twisted the hand holding the box back and forth as he spoke.

"Naw, we've got plenty at home. I'm only interested in réal that I can trade easily. Not everyone has a woman in their life, but people always find a need to drink." Hugo thought briefly. "I'm looking for razors as well. Small, portable. The guy I buy my flour from is always happy to trade those."

"I get razors I use them to keep my own face looking nice Hugo." Hector frowned. "How about paper? Let's do six pork buns for ten dollars?" Hector reached into his pocket and pulled out a single bill. It was rough worn and creased, but unmistakably a piece of currency.

"Hector, I would be happy to give you four pork buns for that." The value of paper against réal varied.  Paper was easier to transport than trade goods, but also much harder to make use of. Hugo waited for the offer.

"I'll take it. Thanks Hugo. Tell Felix I say hi." Hector handed the bill over to Hugo, who checked the denomination and boxed up the pork buns.

As the day went on a few other people stopped by to purchase buns. No-one else had paper to trade and the space below the table in Hugo's stall got more crowded as he accepted toiletries, cigarettes, and a single bottle of rotgut vodka.

# Chapter 2 - Home

By 3 P.M. Hugo started closing up. He'd run out of baked goods to sell and he had plenty of réal to haul home. After taking down the tarp covering his stall and packing everything into his wagon, he began to walk home.

The park quickly receded in the distance as he headed down the street. Palm trees, the ubiquitous signifiers of LA as an exotic destination, sagged heavily from their tops. The trunks of the trees blended in with the shades of brown on the buildings Hugo walked by. He read the familiar signs as he passed by each building.

"Park outside pool inside, 85% renter satisfaction rating."

"Owner on the grind wants to build community in 35% cap rate building. We're all going to make it, together"

As Hugo came to a building, built with brown bricks and surrounded with a metal fence, he read it's sign: "Availability! Save on rent with our low cost e-management" He slid his key into the building's front door and unlocked it before heading up.

A second key unlocked the bulky apartment door lock. A disheveled middle aged man was sitting on the couch next to a neatly folded blanket. He wore a collared shirt, greatly rumpled, and simple blue jeans. "Hugo, good to see you man! How's sales today?" the man asked. 

"They're going well Ernie. I got a bottle of Vodka today." Hugo lifted the bottle out of his wagon and showed Ernie.

"Yuck, cheap Russian rotgut. Better off using it to clean floors. Dilute it and add a few drops of scented oil." Ernie made a face as he said it. "Which yes Felix, it's not regulation, but getting your certification and working are two different things. You know that."

"Yes yes Ernie. That might work fine at the homes you clean, but I already have an office cleaning certification. They will denylist you on the spot if they catch you with alcohol in a work environment." Hugo's father came out of the adjoining kitchen area. A medium height man, he had a phone in his hand and was scrolling through it. "Come here son." Crossing the room he gave Hugo a hug with his free hand, following it up with a kiss on the forehead.

"All these different certifications are a scam anyways." Violet came into the room as well. Hugo's mother, she sported blue hair and was just a little bit shorter than Hugo's father. She too scrolled on her phone. With her free hand she munched on a peanut butter bun. "Hugo, these buns are great. I hope you don't mind me eating one. I gave you life after all."

Hugo rolled his eyes. "Just one Mom, I brought back some razors like you asked." Violet slipped her phone into her pocket as Hugo handed over a small pack of safety razors. "Just so you know, these cost three peanut butter buns."

"Thanks so much sweetie." Violet smiled as she took the razors from him. "I hope that you're eating as well. No one trusts a skinny baker." Turning back to the men. "Every year, they have us pay to recertify. And then you need a different certification to clean an office than a house? What's the difference anyways?"

The men chanted in unison: "House cleaning focuses on detail. Office cleaning focuses on speed." Ernie high-fived Felix. "I am teaching you well my friend."

"Uh-huh." Violet finished the bun and took her phone back out. "I have a 6 PM offer for baby sitting. This is a toddler and yesterday I watched over an infant." Violet cocked an eyebrow at the two men and smiled a little. "Good thing I don't need separate certifications for those." Swiping her finger across the screen, she accepted the job and put her phone back into her pocket.

"Sure, but you still certify for French every year. It's why you keep on getting those jobs. They think a few hours with you and their child will be multilingual." Felix smiled back at Violet.

Violet replied back in Chinese. Casually shrugging both her shoulders and with a roll of the eyes she started helping to unload Hugo's cart. "Chinese... is more useful?" Hugo struggled to translate.

"You got it right." Violet smiled at him. "I'm glad you are learning." She moved the items off the cart. The living room had piles of different réal, with some loose attempts to group them by type. Most were in boxes but a few were just left loosely on the ground. "Really honey? Caramel corn?" Violet lifted up a long plastic bag, sealed at one end with a twist tie. "This will only last a few days before it goes stale. You're saving up to open a bakery, not provide a community service."

"I know Mom." Hugo held his hands up in front of him as he explained. "I haven't decided what to do with it. I can eat it as a snack or I can put it on top of baked cookies. It will get used."

"I vote for the cookies" Ernie said. "Seconded" added in Felix.

"Hey now, we like you and all, but you'll still have to pay for them. Speaking of which, did that payment go through to the landlord?" Hugo inquired. "I can't believe they just locked you out of the apartment on the first of the month. Those e-locks on the doors are the worst."

"I know, I hate them. They give us a physical key to open the door and then add some fancy dohickey that keeps us locked out." Ernie frowned. "I did pay, I paid on time like I always do. Some fraud mechanism got tripped and the bank won't complete the payment. I spent three hours waiting in the Meta and couldn't get a teller."

"Ugh. Speaking of which, I need to hop in. City Hall is open and I need to get my business permit." Hugo went to the headset in the corner of the living room and put in on. "Mom, can you finish unloading? Thanks."

"On it honey." Violet continued unloading items. "It's ridiculous that they make you rent the space before you even have the permit."

As the headset settled over Hugo's eyes the lights inside it gently came on. White noise gurgled out from the speakers and a logo appeared. "Meta Block - The World is Yours".

# Chapter 3 - Meta Block

The lights turned up to white, then softened to reveal a virtual world. Hugo took a breath and braced himself.

If you owned property in the Meta, you'd start there. A tastefully decorated apartment, a view from high up, some music playing in the background. Perhaps a virtual dog to run up and greet you. Hugo did not own property in the Meta. His view began on the street, in Founders District.

A hovering billboard showing a long Japanese eggplant was the first thing he saw. The billboard spoke to Hugo and said "Big growth for affordable prices...."

Avatars pushed past Hugo. Streams of beings, each heading their own way.

"By God the end of the world is coming." a man in a priests outfit shouted nearby.

The avatars looked like whatever their creators wanted. Humans in business suits shared space with masculine looking wolf human hybrids. Cat girls with anime styled blue hair strutted as drones with brains in bubbling tanks flew around them.

"Hey Cutie, I'm in MACARTHUR_PARK and I want to meet you" said a voice right by Hugo's ear. He didn't turn to see the bot, and sighed at the change in tone and inflection when it said the name of his neighborhood.

None of the avatars who actually knew each other spoke on the public chat. Any communication was done over private channels. Some relationships could be inferred if a pair moved in unison, but that was like finding two bubbles that stayed together in a roiling river.

The priest continued shouting "Save yourself at the Church of Holy Workers. Let not your body rest, but put it to work. Idleness is a vice!" The voice reached a crescendo at the end.

Hugo flexed his fingers over the chorded keyboard. Each hand rested on five keys. His eyes were immersed in the screen, so he couldn't look at what he was pressing. The combinations of ten keys he could press at any moment were translated into meanings in the Meta. First he activated the ad blockers. The voices, billboards, and the priest all faded away. Hugo exhaled in relief. Next he filtered out the people he didn't have a pre-existing relationship with. The city blocks faded away to emptiness and Hugo was alone with the buildings and empty streets. 

Hugo queried to see how much a taxi would cost. Even in the virtual world, it was too expensive to be worth the time savings. With some final keystrokes Hugo plotted out a route to City Hall. A set of blue bubbles appeared on the sidewalk in front of him and trailed off. Hugo set out and followed the path.

With the people and advertisements gone, the buildings themselves stood out in contrast. Many of the buildings were girthy towers, shooting bolt upright with bristling trees lining their bottoms. They came in a variety of colors, many of them with bulbous penthouse suites at the top.

The salaciously shaped skyscrapers each had their own set of messages as Hugo walked by each. 

"Spacious top level floors provide beautiful views. Ask about our personalized loan program."

"Buy today, sell in a year, retire. Less than 10% of units are available."

"High density housing at low low prices. Save even more with smart contract based dynamic time-division. You don't need to know or see your housemates thanks to award winning multiple access splits!"

"Isn't that just a time share?" Hugo muttered as he walked by.

One of the more tasteful exceptions was a blue, art deco style tower. It's vertical lines rose up and Hugo let his gaze wander over the gold filigree that formed a row over the bottom story and a sun over the entryway. As his gaze reached the end of the building he saw a wolf-human hybrid standing by a simple, one story building next door.

Hugo drew closer and tried to query who the avatar was. With his ad blockers he had to have interacted with it before. It's wolf head, hairy chest, and muscular human arms were not familiar, but appearances were malleable in the Meta. It looked at Hugo and smiled. "Hello friend."

"Hello." Hugo tapped and released keys to start a query. "I'm sorry, I don't recall how I know you."

The building behind the avatar was designed as a simple ranch house. Small buildings meant less units to sell on a plot of land. A single occupancy home was a display of wealth in the Meta. The wood walls and simple design was spoiled by the pumpjacks on the front lawn. Their hammer shaped heads bobbed up and down on the lawn. Instead of oil, money emojis were popping into the air and fading away with each up stroke. Hugo realized what the person was: "Oh fuck me, a crypto miner."

"How you know me doesn't matter." The avatar glanced over it's shoulder and then back at Hugo. "Do you like my house? I bought it myself with the money I earned mining. You know, I teach courses on how to do this yourself. Very affordable."

The query returned and Hugo read it's contents. He had once followed this persons online videos on how to make the most of leftovers. "That sounds very nice, but" Hugo turned and started to leave "I've got to go."

"I know you're in MacArthur Park. That's not a great area."

"Yes, thank you, I know I don't have a VPN. Good job tracing my address."

"You can do better. I live in a great house in the real world. Just try the course, do you want to be poor forever?"

Hugo took a deep breath. He turned back. "What the fuck am I going to buy with some crypto coins? Nobody takes those in the real world."

"Hugo, language" came a muffled voice from the real world.

"Do you take payment in réal? I've got baked goods, that's my path to success. You want to take those for your stupid course?"

The wolf face on the avatar smiled languidly and ran its tongue along its lips. "Bank only. You know how it is. If you don't have digital money, you don't really have money."

Hugo turned his back on the avatar and followed the blue dots with hurried steps. His face was in a grimace as he went along.

When he reached City Hall, Hugo had calmed down. The virtual building was much the same as the real world. A flat lower story lead to a rising tower. Each side of the tower was flanked by smaller towers. When Hugo stepped inside, it didn't seem anything like a real building.

A semi circular lobby opened to 6 branching corridors. The walls were a soft green and the carpet on the floor an office taupe. There were no signs nor receptionists. Hugo attempted to pull up directions to the business permits office. A blinking sign informed Hugo that the City Hall of Los Angeles was a premium zone and asked for additional payment. Hugo closed the map and choose a corridor at random.

As he walked he passed door after door. Each one was identical: a walnut brown door, the top half having a frosted glass window, and a brass doorknob. The signs on each one were different: Department of Electrical Inspection, Adjunct Office of Fishing Permits, Bureau of Home Agriculture, Main Office of the City Clerk. It went on and on. Nothing was numbered so Hugo couldn't tell if he was making progress or not.

Passing by the Sub-Bureau of Weights, Hugo saw another corridor open to the side so he decided to go down that. He turned down a hallway, then down another. After some steps he stopped. He was in another T-Junction hallway, standing in front of the door to the Sub-Bureau on Measures. But this was wrong.

"Okay, let's see. I turned right a little past Weights. Then right, right, and I should be back where I started." Hugo stared at the sign that read "Sub-Bureau of Measures". "This architecture is fucked." he said out loud.

Glowing, transparent, slightly pink walls surrounded Hugo. A chubby, middle aged woman materialized at a desk in front of Hugo. Her avatar looked at Hugo with mild disdain.

"Language! I told you already" said a voice by Hugos ear. 

"Sorry mom" Hugo replied back. He looked at the woman at the desk.

The avatar blinked at Hugo once before answering. "Your language is disrespectful towards the people around you, the city of Los Angeles, and is a violation within city property. This is your first warning."

"Okay, okay I'm sorry." Hugo raised his hands. "You're right, I shouldn't have cursed. I don't get where anything is in this building. It's empty, it's confusing. Can I get a map, or some directions to the Office of Business Permits?"

The lady at the desk frowned at Hugo then tapped out something on her own hands. Hugo's filtering software was overridden and the corridors were thronged with people. The only open space was that delimited by the pink box he found himself in. 

"Sir, the city hall of Los Angeles employs fifty thousand people." This felt like a well rehearsed speech. "You are not the only person here and there are lots of other people who need to make use of government services. Furthermore the hall internal is built on a four dimensional design, with rights to that copyrighted by the architect. You are able to purchase a map for a nominal fee."

"It's a virtual world. Why make it so confusing? I just want to get to the office." Hugo was starting to feel like he'd made a mistake on how he'd started off with this woman.

"Sir, the architecture of this building is an homage to the former real world offices. Now that we are able to streamline services online that building has been put to better use while the government expanded its services to better serve the populace. If we were to allow the hallways to stretch beyond the dimensions of the original building it would reach all the way over to MacArthur Park." 

Hugo sensed a chance to reset the tone of the conversation. "Oh wow, that would be convenient. I could just walk from my apartment right into City Hall!"

The avatar blinked once before answering. It had to be a deliberate emote, with her selecting it from her own chorded keyboard. "I've heard of that place. It's rough isn't it? I guess it explains your gauche behavior." There was some sympathy there.

It was Hugo's turn to blink, but he wasn't going to express that over the Meta. "Ma'am, pardon me, but where are you from? MacArthur Park is pretty close to downtown. Don't you work near city hall?"

"I live and work in Louisiana." she replied. "One of the wonderful things about the online first approach of Los Angeles is that it opens employment opportunities to people like me. I can work for your city while living in an affordable, and safe, location."

The aside about a safe location was not missed by Hugo. He tried to reset the tone again and play a sympathy card. "Please ma'am. I'm lost and I really need to get to the office. It's important for my family. Can you help?"

She looked at him, unblinking this time. "I am here to help. In the future just say 'I am requesting assistance' and a member of my office will be here to help you". Her virtual hands flexed, silently clicking along a chorded keyboard of their own, and the world around the pink box blurred as the box speed through the halls. After a moment the box stopped in front of another identical looking T-Junction. The woman and the walls faded away without a goodbye.

In front of Hugo stood a door "Office of Business Permits." He opened it and went inside.

# Chapter 4 - Certificate of Good Standing

Hugo looked around the waiting room and blinked. It had chairs, and avatars sat in them. A sign on the wall listed the estimated wait time as "1 hr." A receptionist sat patiently at a desk and looked at Hugo but did not make conversation. A bell dinged and one of the avatars evaporated into thin air. The sign on the wall changed to read "58 min".

Hugo went over to the receptionist. "Excuse me sir, how does this all work?"

The receptionist looked at Hugo and then at the chairs. "You sit in a chair. It doesn't matter which one. Our automated system will then note your place in line." Her voice was puzzled at why he would ask such an obvious question.

"Yes, but, this is the Meta. Can't we just have an appointment and come back when it's our turn? Why wait?" Hugo reflected back her same puzzled tone.

"Oh, of course sir." The receptionist perked up, understanding that there was a service she could provide. "Appointments cost 5 dollars to schedule. Once you transfer payment over I will provide you with a slot."

"Oh uh" Hugo needed to save as much bank as he could for the business permit itself. "I have some paper" There was an awkward silence as they looked at each other. "In the real world of course. How does that work?" Another awkward silence ensued.

The receptionist finally broke it. "Sir, we take an online first approach. We can only accept money transferred from bank accounts or a trusted payment processor." The receptionists fingers clacked over his keyboard and a screen appeared in midair. It showed various logos of banks and payment services. "Would you like to pay with any of these services?"

Hugo read over them for a moment, to try and save face. "No, you know what it's fine. I use different services." He turned around and went to the chair. As he looked at it, an option came up to sit in it. He selected "yes" and as he shifted into the seat the sign on the wall changed. "Your personalized wait time: 1 hr 10 mins."

Hugo shifted his body and stretched a little in the real world. He called out "Mom? Dad? Can one of you bring me some pillows? I'm going to be here a while." After a moment a pillow was slipped behind his back. 

Ernie's voice sounded in his ear, "Your dad left for work and Violet is in the kitchen. My shift doesn't start for another hour. You want me to get you some water?"

"Thanks Ernie. I'll pass on the water. I don't think I could use the bathroom with this headset on and I'm scared I'll lose my place if I take it off."

"Parasites." Ernie clapped him on the shoulder. "Stay strong kid".

Back in the waiting room, time slowly counted down. Sometimes it also went back up, but there was nothing to do but wait. Hugo watched another person go to the receptionist and start shouting after a moment. "What do you mean I lost my place in line? My dog needed to be let into the god da-" The avatar disappeared before it completed it's rant. Hugo felt glad he had passed on the cup of water.

After around an hour and a half Hugo's personal timer hit 0 minutes. The display on the sign changed to fireworks and after a moment Hugo found his seat was in front of a desk in another room. Yet another avatar, this one in a suit with a pocket square, sat behind another generic desk. "Welcome to the business permits office. How can I help you today?" Finally, the wait was over.

Hugo stretched his jaw for a moment before answering. "I'm here to apply for a business permit. I am opening a bakery in Los Angeles."

"Oh wow, a bakery, eh. We can always use more baked goods." Hugo couldn't tell from the tone how the human behind the avatar actually felt. "For a restaurant license we'll require a few items. You'll need a food safety certification, employer identification number, let's see," he paused, thinking, actually pulling items from his memory, "and what is the name of your business?"

"Hugo's Savory Baked Goods. I specialize in savory baked items sir." Hugo was so excited he just started sending in his pre filled out forms before the avatar even asked. Papers appeared on the desk on Hugos side and slid across the table to the office worker.

The worker briefly glanced down at them then back to Hugo. "Hugo is your first name, yes?"

"Yes." Hugo began to feel nervous again. The worker had not even reviewed the forms he had labored over in his spare time.

"We'll require a fictitious business name statement. Not to worry, we can do it right here. It's very simple and has an affordable 100 dollar fee."

Hugo suppressed a groan. The amount of money he had saved up in a bank was limited and this was going to eat into it. "Of course, but why? My name is in the business."

"Your first name is. Your family name has to be in there to avoid filing the statement. But like I said, it's very simple to do."

"I'll" Hugo breathed in and out "make it my last name. Castillo. Castillo's Savory Baked Goods. Why spend money if you don't need to, right? Every last dollar counts." Hugo felt like he didn't belong, but he tried to play it off.

"Yes, every dollar counts." The avatar of the office worker didn't appear to move, but Hugo felt like he had said something that had gotten the mans attention. The avatar caused the forms to levitate in front of him, fluttering back and forth as he reviewed them. "Let's see, everything here does appear filled out. MacArthur Park home address I see, right near the Halls physical location. Ah, and it looks like the bakery is walking distance from it. A real community business." The papers stopped floating around, assembled themselves, and laid down on the desk in a neat stack. "Unfortunately you seem to be missing a certificate of good standing."

"A what now?" Hugo hadn't seen this on the website when he was gathering all of his paperwork.

"A personal certificate of good standing. Proof that you do not have outstanding debts, warrants, and a social media review to ensure you are not associating with known gang members. It's on the LA City Hall website."

The avatar's fingers clicked and a screen showed up in front of Hugo. It listed the certificate and the requirements. Hugo's heart nearly stopped when he saw the fee. 1,000 dollars. All of his banked money had gone into saving for the permits and renting a space for the bakery. He could put together some paper money, maybe find some loans, but he didn't have enough to cover the fee.

Hugo held his voice steady as he spoke. "I have paper. I can walk it down to city hall in person. What's the procedure for that?" There was a brief silence as Hugo added on "Sir. Sorry sir, what's the procedure for that?"

"Ehm, paper bills." Hugo could sense the person on the other end furrowing his brow. "You understand, we are trying to move away from paper currency as part of our modernization process. You would need to go a registered bank, provide paperwork showing the money has a legitimate provenance, then make a standard bank transfer."

Hugo didn't know what to do. He thought about the requirements to show the origin of paper money. He couldn't even show legitimate provenance for the ten dollar bill he'd picked up today. Not without first having a business license and then a currency scanner. "That's going to be really hard for me." his voice was soft, subdued, when it came out.

"I understand." the office workers voice stayed flat and even. "There are some programs to help small business owners get started. I'm afraid I've misplaced them, but, if you can give me your contact details I'll send it your way."

Hugo created a contact card and watched it materialize on the desk and slide across to the man. "Thank you for your time today. Sorry to bother you."

Hugo signed off the Meta and took off his headset. Violet sat on the couch looking at him. She'd heard each word he said in the office. Hugo could tell from the sympathetic look on her face. Violet gave him a few squares of paper towel and held him as he cried.

## Chapter 5 - Opportunity

"A personal permit of good standing. Huh." Ernie stood on the other end of the vendor stand. He picked at his teeth as people crossed behind him. "I haven't heard of that one before. Those parasites will put a tax and a permit on everything they can if it keeps them employed."

It had been a few days for Hugo and the initial shock had passed through him. "I don't know what to do about it. I close my eyes and I see that price tag he showed me. One thousand US dollars. Even if I had that much paper, how could I make it into Bank?"

"You couldn't." Ernie said. "Those anti-drug laws. You take paper from the bank, you can trade it around. You want to put it back in the bank, they make you show how you got it. They even make you get one of those serial number scanners if you're a business. It's all a scam." He grimaced. "Anyways, look, I appreciate your parents helping me out, but you know them. Goodness of their heart, they won't take anything from me for it."

"Come on Ernie, I've known you so much you're pretty much an uncle to me. I never heard, did the bank ever tell you why they finally paid the landlord?"

"No, they don't have to, so they won't. I'm just glad my door opens again and I can put on some of my own clothes." He held up a bottle of cheap vodka, a big one. "I found this in a corner of my apartment and I thought your dad could use it. He just got his house cleaning permit and this will go further than some lemon scented all natural spray. How about..." he waved his finger around before settling on a pastry "one of those hot dog buns? Got to eat on the run."

"Ernie, I told you, you don't have to do this." Hugo protested.

"Kid, I told you, I got places to be. Let's make this fast." Ernie set the vodka down on the tabletop. "Now come on, hand me that bun before I get it myself and violate some food safety law." For all of his bluster, Ernie had a small twinkle in his eye as he spoke.

Hugo lifted the bun up and put it in his hands. "Thank you Ernie. Pleasure doing business with you."

"You too kid." Ernie walked off munching the bun. Hugo smiled at him as he walked away.

After a couple minutes Hugo sighed and the smile left his face. He checked his phone. There was a message from a sender he didn't recognize. "Hi Hugo - this is Frank from City Hall. We met when you wanted to get your business license. Can we talk in person?"

Hugo remembered the avatar at the desk and how he'd mentioned an assistance program. He licked his lips and texted back: "Yes, that would be wonderful. Do I meet you at City Hall in person?"

"No, we'll meet at Dancing Goat Coffee, tomorrow at 10 am. Bring a pastry." A picture of the mans face was attached. Middle aged, and white, he had a goatee and large bags under his eyes. He wasn't smiling in the photo.

The next day Hugo came into Dancing Goat. A few blocks from City Hall, it felt excessive for a coffee shop. Hugo looked around at the mid-century furniture and polished stone coffee tables. A yellow neon sign next to some ivy said "hello sunshine". For an extra 2 dollars he could have something called "myracle mylk" in his coffee instead of the soy milk he was used to. Looking around he saw Frank at a table. Frank waved and smiled briefly. "Hugo, get yourself something to drink." he pointed at the counter.

Hugo went over and paused looking at the counter. He'd had coffee at home plenty of times. Sometimes when his mom had a great day she'd take him to Starbucks for a frappuccino. He'd never seen anyone lay out coffee beans from different parts of the world in separate display tins. Each one had little plastic figurines of goats mixed in with the beans.

"Hi! What are you in the mood for today?" Hugo stopped trying to figure out if a bean from Guatemala looked different from an Ethiopian bean and looked up at the barista. Smiling and young, with perfect white teeth and curly hair peaking out from under her cap, she exuded energy. A nametag pinned to her apron read "Whitney".

"What's good here?" Hugo sounded out each word slowly as he scanned the menu. Charcoal latte's couldn't possibly taste good. He wasn't quite sure what would go into an espresso tonic. An ordinary coffee was there, but it felt like ordering that would signal he wasn't sophisticated.

"The rose latte is my favorite" the barista replied. She nodded a couple times as she mentioned how it "had a light taste and good balance."

"Okay, I'll take that. How much is it?"

The barista blinked at him. "What sort of beans sir?"

"Oh! How about Guatemalan." Hugo replied back. He still had the ten dollar bill in his pocket so he took that out to pay.

The barista took it and put it into the currency scanner. A laser whirled along the bill and the readout listed it's characteristics:

- $10 bill
- Genuine
- **FENTANYL DETECTED**

The last line blinked on the screen. Hugo looked back at the barista and blinked. He rubbed his hand on his pants.

The barista shrugged. "It's weirder if nothing is detected." She put the bill into the till and handed him back some change. Raising her eyebrows she leaned in a little "I wonder what drugs are on these?" Hugo laughed and dropped a couple single bills into the tip jar.

The drink looked exciting when Hugo got it. Rose buds added bright red spots on top of a layer of thick white foam. The drink itself felt warm in his hands.

Sipping the latte he joined Frank at the table. A rose petal got stuck on his lips and Hugo put it back in the cup. As he sat he put the bag with pastry down as well. Frank looked him up and down. "You're skinny for a baker Hugo."

"I'm just getting started. Come see me in five years." Hugo smiled and pushed the bag across the table. "This is a pineapple bun. There's no pineapple inside, that's just the name, cause' of the texture. It's a sweet bun."

Frank pursed his lips and looked inside. "Thanks." He took it out and started eating it. "Didn't you want to call yourself a savory pastry shop?"

"Well, I'm the best at savory pastries, but who doesn't like sweets?"

"You're unfocused Hugo." Frank set down the bun, half eaten. "But you got heart. I wanted to talk to you about your permit issue. You seem like you don't got a lot of savings to get this business started. That true?"

"The permit of good standing? Yeah, I mean, I've heard of that for a business, but never for a person. I don't get it, I never even saw it on the site when I was putting the documents together."

"Yea, good standing, that's the one." Frank paused. "How much does this bakery mean to you?"

"It means the world Frank. I've watched my parents work gig jobs ever since I was little. I really want to own my own business. If I can pull this off and really earn some money, I won't be like them. I can save more and I can even help them pay the rent. Maybe someday we'll even own a house to live in." Hugo leaned across the table towards Frank. As he talked about owning a house, the corners of his eyes turned up.

"Hmm." Frank leaned back a little. "So what would you say to picking up some extra work?"

"Would that help?" Hugo furrowed his brow. "I mean, I need this permit. But when you said there is a program, I thought it would be, like, a scholarship."

"A grant? No, this isn't communism. I work with some people, and they need help with conducting some business. You'll get paid in Bank. Save up enough and you can pay for your permit." Frank tore of a piece of the pineapple bun and eyed Hugo.

"Okay." Hugo thought about it. "And what does this involve?"

"It's pretty easy. My colleagues have paper. Much like yourself, they need Bank. Unfortunately, converting it requires a lot of time and energy. That's where you come in. You follow?"

"Uhh, you want me to fill out the forms that say where the paper came from?" Hugo wasn't sure where it was going.

"No kid, forms take a while. Sometimes the bank freezes assets. The loophole is, if someone buys something from you, they can pay you in Bank. Now, a pastry isn't worth that much" he held up the remaining quarter of the bun "but with enough paper you can buy bigger things. Booze, clothes, maybe even some jewelry. Then you sell it to someone else. Bam, you made bank."

"That doesn't seem quite legal." Hugo was sitting up straight now. He worried that he was being tested somehow. Maybe the permit of good standing had a test where you had to refuse to do something illegal?

"Look, I work with people in cash businesses. They have a need to pay their rent, just like you. These permits are a pain. Currency scanners charge monthly fees. And for what? You and I both know these drug laws are bullshit. I saw you get nervous with that cashier lady when your bill came up dirty. She still took it didn't she?" Frank leaned in himself. "I'm just trying to facilitate free trade and help people out. Let's give this a try. I'll give you a hundred dollars. Buy me a bottle of liquor from the store across the street." Frank reached into his jacket pocket and took out an envelope. He placed it in front of Hugo. "I'll be right here. Don't take too long."

Hugo sat there, thinking. This felt like it was going to far to be a test of his character. He reached over and looked inside the envelope. Several paper bills were inside it. He looked up at Frank, who smiled with his teeth closed and nodded. Hugo closed the envelope and thought. He thought about his parents, and about the constant struggle to make rent. He thought about wanting to prove that he could be successful. He got up and crossed the street.

Buying the alcohol couldn't have been easier. Hugo went and pointed to one behind the counter. The cashier didn't bother asking for ID and scanned the money in. Drug alerts flashed on the scanner as the cashier bagged the bottle and handed it back to Hugo. Each exchanged a "have a good day" and Hugo crossed back.

Hugo placed the paper bag on the table next to his pasty bag. Frank took the alcohol out and placed it where he could get a clear picture with his phone. "Hugo, you've done this before. You send me your bank account. I take a picture of what I am buying from you and send you the money. Show me your code."

Hugo too out his phone and thumbed through it to reveal the QR code for his bank account. Frank scanned it. "Now read me your verification phrase Hugo."

Hugo thumbed his phone again, revealing a set of five random words. "sierra kilo hotel golf X-ray".

"That's not what I have. Shoot, I think my phone picked up the menu code on the table here." After trying a second time, the phrase matched up. Frank tapped out some numbers and then hit send. He looked Hugo in his eyes. "This is part one. Now I need you to send the money to someone else."

Hugo looked at his phone and saw he'd received ninety dollars. "This isn't the full amount Frank. You gave me a hundred."

"I know what I gave you. This isn't about haggling for the best price. You want to make sales, and the fastest way to do that is to sell at a discount to the real value." Frank held up a QR code on his phone. Hugo scanned it and read back the code. It belonged to a business named "Comprehensive Dental Solutions". 

Frank gave him a tight lipped look. "Okay, now this is the important bit. You need to wire them seventy dollars."

Hugo punched in the numbers on his phone and hit send. "Okay, that's done. What happens to the remaining twenty?"

"That's yours Hugo. You're working with us now. You're going to be doing this a few times. Not with me, but just like this. We'll tell you where to buy and where to sell. This will just be a little while, until you have enough saved up."

Frank put the alcohol in his bag and gathered up his possessions. He stood up to leave. "Where is a good place for us to find you?"

"I run a stand by MacArthur park. Maybe your associates can" Hugo paused, uncertain, "buy some pastries?"

"Oh one of those unlicensed stalls? I know where that is. We'll be by." Frank smiled and nodded at him, then left.

Hugo finished his coffee and looked at the dregs of the rose buds at the bottom. Soaked with foam and coffee, the buds looked like they had fallen to the ground and gotten covered in dirt. Hugo placed his coffee cup in the trash and left.

# Chapter 6 - Earning

The man at the stall didn't smile. He didn't even try to have any sort of greeting. He came up, somber faced, in his black leather jacket and blue jeans. His conversation opener was "Hugo." He then pointed at a random pasty and ended the conversation with "that one." Hugo bagged it and took the roll of money from the man. Getting him to take the pastry and not just shove money at him on the street had been a victory.

Hugo stuck the money in his pocket and worked for a bit longer before wrapping up the day. After heading home Hugo slipped into his bedroom and counted it out. A thousand dollars. He checked his phone and a message blinked on it. 

"Locations are attached to this message. You're going to get a watch. Tell the store clerk you want a 'big bling deal'." A map showed a pawn shop and what looked like an apartment building. As Hugo read it, a countdown timer started. He had one hour before the message went away. That would be plenty of time.

Hugo came into the pawn shop and looked around. He found a pair of watches that added up to around the amount. Pointing them out, he asked the clerk to ring them up. When he pulled out his cash, the clerk glowered at him.

"No paper transactions over fifty dollars. Store policy." The clerk tapped the sign with his finger and started to put the watches back.

"Hey, wait a minute." Hugo stopped him and remembered the last part of the message. "I heard this was a good place for a.." with a pause "big bling deal."

The clerk looked him up and down and grimaced. "Do you not know how to work that into a normal sentence? Fucking hell man. And why didn't you start with that?" He reached under the counter and brought out a wristwatch with a giant face. It's bezel and bracelet were covered in jewels.

Hugo looked at it. "It's not running." He pointed to the stopped second hand.

"That's not the point. Give me the paper." The clerk held out his hand.

Hugo handed the paper over and waited for the clerk to bag up the watch. The clerk nodded at it and finally pushed it towards him. Hugo pocketed the watch and left to find the buyer.

Looking up the address on his phone, it seemed familiar. Everytime was different but this place he'd been to before. After knocking on the door a man opened up and let Hugo inside. "Hey, Paul right?" Hugo said. "Good seeing you again."

Paul was unshaven and wearing a white tank top. He glowered back at him. "Yea, listen, let's get this over with." Hugo set the watch down on the table, gave Paul a smile, and gestured with his hand. 

"What the fuck kid. Give me your account details." Paul looked at Hugo like he was retarded.

Hugo took out his phone and frowned. "I gave them to you last time, you should have it saved." He thumbed out the QR code and held it up for Paul.

"Look, I don't save evidence on my phone. I'm not stupid. The police come looking, I'm not going to have a bunch of goddamn evidence all lined up for them." Paul took a picture of the watch.

"Uhh Paul, don't you want to check the confirmation code?" Hugo looked at his phone "Did you get Whiskey Oscar Echo..."

"What the fuck, do you see any other QR codes around you? Are people putting QR codes in my home, which I live in? Do you not think I can point my phone at yours? I scanned your damn phone it's the only thing with a code in here!" Paul's eyes were bugging out at this point.

"Yea okay look it's cool." Hugo held his hands up, calmingly, and his phone dinged.

"I gave you your money, get lost." Paul pointed towards the door.

Hugo kept his hands up as he went to the door, lowering the one not holding the phone to open it. After stepping outside he checked his phone. Six hundred dollars. He sent all but fifty of it to the account Frank gave him. "Jesus Paul. You sent me the money. The banks going to hold onto those records forever. What evidence are you hiding? And what are we doing where the police are going to care about it? They don't care about anything less than millions." The questions were rhetorical. Paul wasn't around and Hugo was talking to himself as he headed back home to get some rest. 

When Hugo came in through the door to the apartment he found his mom yelling at his dad.

"Oh look Felix, our door still opens, our son can still come home!"

Felix sat on the couch, looking down and playing with his hands. "Ernest said it would be fine. That he'd done it all the time."

Violet sounded out each word as she spoke. "You put it in a different goddamn bottle". She threw her hands up as she turned to Hugo. "Your father has gotten himself kicked off the gig network. I'm going to have one hell of a time making rent on my own."

"Dad" Hugo looked at his father as he sat there forlornly. "What happened?" 

"I used the bottle of vodka for cleaning. Just like Ernest said. Dilute a bit, add some lemon scented oil." Felix didn't look up. "But I didn't put it in a spray bottle. I kept the bottle of vodka and would just put a little on a rag. I didn't think they would care."

"Oh dad, you absolute idiot" Hugo sat on the couch and hugged him.

"They reported me Hugo. Took a picture of it for evidence. Not only did they kick me off the network, they charged me the fee they refunded to the homeowner."

Violet sighed and went over to Felix. She hugged him and put her forehead on his. "Yes Felix, you are an absolute idiot. Now we have to deal with this."

"I don't know what to do now. Gig work was steady. I can't use them as a reference, not after this though." Felix paused and thought. "It will be a while before I can find a job elsewhere. I'm sorry, I really made a mess of things."

"Mom, Dad, it's going to be okay." Hugo paused.

"I know honey. I'll work some extra shifts. Felix can cook and clean around the house. We'll trade in some of your réal for bank." Violet smiled at him.

"No, it's going to be okay. I can cover rent this month." Hugo looked at both his parents. They stared back, confused.

"Hugo son, I know you've been going out a lot, but you haven't been coming back with a lot of stuff." Felix said. "How would you have that much bank?"

Violet furrowed her brow and frowned at Hugo.

"I have the bank, okay. I have it, I'll transfer it, we're going to be fine this month." Hugo got out his phone and sent the money to the family account. Both Felix's and Violet's phones dinged. They checked them and looked at Hugo.

"How?" was Violet's first response. "Honey, is this money clean?"

"Please don't ask me about it. I don't want to discuss it." Hugo replied evenly. 

"Son, what have you been doing at night?" Felix asked. "Please don't tell me you've been dealing drugs. That is dangerous stuff." He began to sound worried.

"Like I said, I can't discuss it."

"Hugo, you need to tell us where this came from. What are you doing?" Felix was sounding more frantic than when he talked about his firing.

"I have some money and I can provide for you guys. I'm an adult. I can provide too." The last part came out louder than Hugo intended. He stopped and looked down.

Hugo's parents looked at each other. Violet responded after a moment. "You know we love you. This means a lot to us right now. We have to worry though. You mean so much to us."

"I know. You mean a lot to me too." Hugo hugged both of them. "I'm going to bed."

Hugo got up and left the room. He sat on his bed for a bit listening to his parents worry to each other about the source of the rent money. "I thought it would be different." he whispered to himself. "I thought they'd be more proud." He pulled out his phone and sent Frank a message. "I'm going to need more jobs. Something came up." He then laid down and went to sleep.

# Chapter 7 - Big Purchase

The next few months operated on a steady rhythm. Hugo would run his stall until lunchtime. About once every week one of Frank's associates would drop some paper off. After closing down his shop, Hugo would go through the process to trade the paper for some trade goods, then sell them to get digital bank. He'd forward the bank, minus his cut, to Comprehensive Dental Solutions. Sometimes he'd grab a coffee with Whitney at The Dancing Goat. She was pretty fun to talk to, even though Hugo didn't get romantic vibes from her.

After the end of one of those excursions, Hugo came back into the family apartment. It was evening and near the end of the month. Before he even came into the house, Hugo had already forwarded some bank to help with the rent.

Felix was sitting on the couch when Hugo came in. He put down his phone and looked at Hugo with a serious face. "Hugo, we love you and this really helps. But I have my own job as well. Selling fast noodles is steady. You don't have to worry."

"I'm not worried, Dad." Hugo replied. He put down his backpack and sat on the couch next to his dad. "I want to help out. I want to be a man, just like you."

Felix patted him on the shoulder and smiled. But his smile was thin and worried. "You're still saving up for your bakery right? Whatever this is, can you stop it afterwards?"

Hugo assured him he could. They made small talk and Hugo went to bed.

The next day Hugo watched his usual contact approach his stand. He was carrying a small paper baggie in one hand. "Oh, did you stop at another stand?" Hugo asked, attempting once again to strike up conversation.

"No." The man, Hugo still didn't know his name, put the paper bag on the stand and then grabbed a pastry off the tray.

"Hey, you can't do that. I handle the food." Hugo started to protest.

The man looked Hugo in the eye for a moment and nodded once, briefly. "Good luck. You make good bun." He walked off without any elaboration.

"What the heck" Hugo muttered and thumbed open the paper bag. He stopped and closed it quickly. Then he opened it once more and looked at the quantity of money inside. He closed it quickly and shut down his stall.

As he walked back home he messaged Frank: "Is this the right amount? I think your man overpayed."

The reply arrived before Hugo made it home. "It's correct. You're going to be buying a car. Pick up and drop off are attached. Be there at 10 AM tomorrow. You'll drive it straight to the drop off, no stops." Hugo frowned and assessed the pick up location. It was in Lebec. Nearly 75 miles north of him, and in the mountains outside of Los Angeles. He could take a bus there, but it was going to take several hours.

Hugo left his stall contents in the living room and briefly waved at his parents as he went into his room. After closing the door, and locking it, he counted the money out on the bed. Seventy thousand dollars. This was an enormous amount of money. "I could start my own bakery tomorrow with this. And help with rent." Hugo muttered to himself. "What have I gotten myself into?"

Laying down in bed, Hugo kept on thinking about the next day. He got up and laid out some clothes he could layer. He laid back down. After a while, unable to sleep, he double checked the bus route he would take tomorrow. He got up and rechecked his clothes. He made sure the bag of money was at the bottom of his backpack. Finally he slept.

# Chapter 8 - Drive

Hoping off the bus in Lebec, Hugo looked around. He'd arrived by the post office. A single story building, the post office had enough space for 30 cars and 5 spots taken up. It looked like the biggest parking lot he could see as he turned around in a circle. Dirt lots, their dimensions delimited by wooden posts and chains, marked other properties. Dirt hills, the plants on them dead, were all he could see in the distance.

He breathed in the cold air and shifted his backpack around. His breath turned into mist as he exhaled. A man in the parking lot was sitting on the hood of a car, smoking a cigarette. The car sat on the far end of the lot, away from the other cars parked closer to the post office. Despite the cold, he was wearing a tank top, emblazoned with gold baroque  patterns and a drawing of Medusa in the center. He wore large sunglasses, so big they obscured the middle third of his face. Seeing Hugo looking at him, he waved.

Walking over, Hugo looked at the car. It looked like an older Mustang, white with a boxy exterior. "Hey, nice car." he said as he got closer.

"Hugo." The man had an accent that sounded Eastern European. "You got the money?"

Nodding, Hugo took the bag out of his backpack and handed it over. The man didn't bother checking its contents as he pulled a set of car keys out and handed them over.

"Do you, uh, need a ride?" Hugo asked awkwardly. He looked around and didn't see anyone else watching them. The ground was flat and visible for a mile around, excluding the few buildings nearby.

"To the city? No." The sunglasses hid whatever expression the mans eyes had. They were so large Hugo couldn't even guess what expression his eyes had. His mouth was flat and neutral. "Don't stop driving." With that said, the man walked off.

Hugo got in and started the car. He didn't have a license, much less a car, but his dad had taken him driving a few times before. He did a couple loops in the parking lot to get familiar with the Mustang and it's throaty growl before going back on the highway towards Los Angeles.

Driving back he thought everyone was taking the "don't stop" too seriously. "What if I need to pee?" he laughed as he speed through the mountains. "I don't think they'd be okay if I just let it rip all over these leather seats!" He knew the perfect place to stop too. He pulled out his phone and texted Whitney to see if she was working today.

Passing by The Dancing Goat, Hugo was unable to find parking in front. He pulled into a nearby parking lot and then realized his issue. You can't show off a car to someone in a parking lot a block away from them. Hugo snapped a photo of the car with his phone, then went into the cafe.

Whitney smiled and waved as Hugo came into the shop. Turning to her co-worker she said she was going to take a 10 minute break. A few minutes later she was sitting with Hugo at a table with a couple coffees.

"So, Hugo, where's this hip car of yours?"

"It's uh, I couldn't get a parking spot in front. It's in a parking lot, but I took a picture of it." Hugo pulled out his phone and showed it to her.

"Wooah, nice" Whitney practically whistled when she saw it. "My dad loves muscle cars. He's always tinkering with his vintage Shelby on the weekends." Her eyes danced as she looked at him and leaned in. "What kind of a loan rate did you pay for that? It's such a splurge."

"Oh, it's" Hugo couldn't come up with anything "no loan, all cash."

"No way! When did you get the money together for that? That's, what, a 40k car?" Whitney looked very impressed.

"Well, uh, where there's a will, there's a way." Hugo laughed awkwardly. They made some small talk for the next few minutes, but in the back of Hugo's mind he thought about how much he'd paid for it. That wasn't a discount on it's value. That was almost twice what it was worth, brand new.

Finishing up with Whitney he headed back to the parking lot. Entering the stairwell he passed a man at the bottom, playing on his phone. The man watched him as he walked by and didn't say anything. Coming to the floor where he'd parked, Hugo saw a startling sight.

Several police officers were by his car, which had the trunk popped open. A wrapped bag was on the ground next to the car. Hugo stood by the stairs for a moment, his heart pounding, and then he turned around. The man from the bottom of the stairs was there and he had a gun out. It was pointed at Hugo.

"LAPD, hands up and get on the ground!" the gun was aimed at Hugo's chest and the man had a two handed grip. Hugo turned back towards his car and saw that the officers searching it where also brandishing their firearms. Surrounded, he put his hands up and slowly got to the ground.

# Chapter 9 - Dirt

He'd been sitting at the desk in the interrogation room for nearly 20 minutes before his interrogator came in. Hugo knew that because the only thing to do was look at the clock. He would have liked to pace around, but being handcuffed to the desk prevented him from doing so. Cameras stared down from several angles, silently judging him as their red LEDs blinked on and off. Nervously, Hugo tapped out a rhythm with his feet.

"You're in big trouble Hugo Parrish. Big fucking trouble." His interrogator was the person who'd pointed the gun at him in the stairwell. The man was wearing a police badge with "DETECTIVE" emblazoned in letters along the top.

Inside his head, Hugo thought "My parents are going to be so disappointed in me." Externally, he just nodded. "I know." He'd had a while to think. Something was in that package in the trunk of the car. He didn't know what, but he must have been followed for the police to have gotten into the trunk when he stopped.

"You had a lot of, what do you people call it, réal, in the back of the trunk." The detective paused and waited for Hugo to respond.

Hugo had never been arrested, but he had seen Goodfellas. That scene where the protagonist gets arrested and doesn't say a thing. Hugo also said nothing and looked up at the cameras. He noticed the lights had stopped blinking on the cameras. "Great, recordings off, now I'm going to get beaten." he thought to himself.

After a couple minutes of silence the detective resumed. "We got a whole kilo in there. Enough opioids to kill 50 million people. You don't work with us, you're never going to be a free man."

Now Hugo had to roll his eyes. "Look man, I know you cops like to make up numbers, but 50 million is a lot of people. What, I'm going to kill the whole state of California? There's nothing in the car that could do that."

"Don't play dumb shitbird. You know what was in there, just like you know you're working for the cartel."

Hugo paused. What were they trying to pin on him? "I'm not working for any cartel. I have no idea what you're talking about."

"Come on Hugo. Comprehensive Dental Solutions. CDS. Cartel de Sinaloa. Could you have been more obvious?"

Hugo felt himself break out in a cold sweat. He decided to keep on channeling Goodfellas and not say a thing.

"We've been following you for a while. You transported that carfentanil in the watch. It must've been a hit, because you're now transporting a whole bunch in that car we just impounded."

"I don't know what's in that car, but fentanyl is not going to kill 50 million people." Hugo responded evenly.

"Carfentanil. Surely they tell you what you're transporting. One hundred times the strength. Top notch réal, réally." The detective chuckled at his play on words. "You get that to a safe spot, you can cut it real nice, and make a whole mess of money."

The detective paused and looked at Hugo. "And you do need money. Your alcoholic dad losing his job couldn't have been easy. We have a whole lot of dirt on you-"

"My father is no alcoholic." Hugo stood up, his voice and posture firm. His hands made fists as they wrestled impotently with the handcuffs keeping him from slugging the detective.

"Sit back down now or I'll knock your ass to the ground" the detective shouted back.

Hugo glared back at him, defiant. "You take back what you said about my father."

"We have a picture of him at work with a vodka bottle. We know he was laid off. We know you paid the rent. We know every damn bit of how you interact with the system." The detective was practically frothing at the mouth. "You think we're stupid? We pay the corporations and they gladly hand it over. The Infoshare program is the greatest goddamn thing to happen for policing in this country and we have you dead to rights. Don't fuck with us."

"The only reason you can even afford to pay for all that is because you're nickel and diming people like me for everything. I just wanted to run a bakery. What the hell do I need a personal certificate of good standing for? When does it ever end with you parasites?" Hugo was leaning over the table, shouting back at the detective.

"What the hell is a personal certificate of good standing? I've never heard of that." The detective shook his head and squinted at Hugo.

Hugo paused. A few moments passed and he sat down.

"We have the dirt on you kid. You're going to go away if you don't fix this. Don't you think you should call someone?" The detective reached into his pocket and handed Hugo back his confiscated phone.

Hugo raised his phone up, facing away from the detective. He unlocked it and looked at the screen. He didn't know what to do. He tried to think which of his parents would be home now. He thought of which one might take the news better. Then he saw he had a message from Frank. He opened it and read it as the 5 second timer counted down before it was destroyed. "I heard what happened. Help is on the way. Don't tell them anything, don't talk to anyone."

Hugo locked the phone again and set it facedown on the table. "No, I don't think I should." He looked down at the table himself and waited.

# Chapter 10 - Reveal

The lawyer calmly weaved his car through the freeway traffic. "It wasn't cheap to get you out Hugo. I should tell you, my boss is pretty upset."

Hugo stared out the passenger window. The setting sun was shining in his face, but with the haze he was in he didn't feel compelled to block it. "Why'd you spend so much on me then?"

"Liability purposes." The lawyer was an African American man in his mid forties. With his tweed suit, wireframe glasses, and succinct speech, he looked like he did insurance law. Not, whatever branch of the law even attempted to cover what Hugo was caught up in.

"So what now?"

"You meet the boss. He's spent a lot on you. He'll want to see how he can recoupe his investment."

The rest of the ride passed in silence until they arrived at an office building in the downtown Fashion district. A metal gate opened to give them access to an underground garage. A guard station stood before the elevator leading up and out of the garage. A guard came out and called the elevator for them. Hugo noticed that the guard was resting his hand on the handgun holstered at his hip. Inside the guard station he could see an assault rifle in an open weapons locker.

In silence they all rode the elevator up. Two stops were lit up. At the first the guard told him "You're the second stop" and rested his hand on Hugo's shoulder. 

The only thing the lawyer said to him was "Good luck" and with a brief nod he left.

Moments later the elevator arrived at its second stop. Hugo walked down a well lit hallway with plants and generic corporate paintings. At the end of the hallway was a double set of wood panelled doors. The guard opened the doors and gestured at Hugo to walk through. As Hugo entered the room the door was shut behind him.

"Hugo, come in! Take a seat." A smiling man gestured at a chair in front of his desk. Hispanic looking, and well into middle age he looked bland. He had a fat face, and a thick mustache. The top of his head was well advanced into male pattern baldness and he wore a loose fitting white dress shirt with no tie or jacket.

Hugo sat down in the chair.

"I have to tell you, those cops are a real pain in my backend. Given what you were caught with, it took a very big payment to get them to release you." The man emphasized the word "very" when he spoke.

"I thought that the lawyer got them to drop the charges."

"Hugo, don't be naive. There's things you can and can't say and that's what I pay Paul to handle. At the end of the day, all they want is a bribe. I view myself as a facilitator of commerce. My business turns loose paper into approved banked money. Sometimes the police ask questions so we give them some of that money and they buy their wife a handbag and send their kids to college. It's a great system and it works well."

"Ok. So then Mr.." Hugo paused.

"Luis."

"Mr. Luis, why the drugs?" Hugo finished.

"Ah, well, I facilitate commerce. Normally I help my clients manage their paper, but sometimes I help with associated shipping issues. This one client has been experiencing some supply chain issues so they wanted to explore alternatives. I'm a local to the area so they asked for my help."

"So then.. the watch with all the diamonds on it?"

"Shiny pieces of glass." Luis waved his hands dismissively. "The key part was the product that was placed in the case of the watch. We had to take a couple parts out to fit the product in, but it was very easy to transport."

"Why have me go to the pawn shop then? Why did I go so far north to get that car?" Hugo didn't understand why he was involved in this. "Why didn't you just mail everything?"

"Cutouts and jurisdictions Hugo." Luis leaned in as he explained what role Hugo had played. "You were a cutout, a person who acted as the middleman because our chemist friends could not be seen directly talking to us or the client. The watch as a means of transport worked well. It passed through a couple of hands and performed well. Unfortunately we hit issues afterwards." Luis frowned at this part, but paused rather than elaborate.

"And the car?" Hugo persisted.

"Jurisdictions. At that point the LAPD understood what was going on and they wanted a very large bribe." Luis emphasized the word _very_ as he spoke it. "But that wasn't going to happen without evidence and there are rituals one must observe. The LAPD can only operate within the city, and they can only enter private property" Luis waved his hands around "with a warrant. Now, they trade information with their friends in the sheriff's department as well. Some of the best customers of my clients live in LA county, outside of the city. But Lebec, where you got the car, is outside of the county jurisdiction. A demon may have absolute power inside it's circle, but it cannot leave it."

"Then why not call the FBI? Or get a warrant?"

Luis leaned back in his chair and crossed his arms. "You're not following along Hugo. These police are not interested in the rule of law. They want to support their families. Warrants, Policía Nacional, those things just let you close cases and tick some numbers off. If all they care about is metrics, they may as well crack down on unlicensed pasty stalls to get their numbers up."

Hugo digested this information. As he did so Luis leaned forward "So why did you stop at that parking lot? What did you do?"

"I uh, met a friend for coffee. I thought she might be impressed by the car." Hugo shifted uneasily in his seat.

"And this friend" Luis raised his eyebrows "what is her name?"

"Claire" Hugo lied.

"Claire. Is she pretty?" Luis had a conspiratorial smile and a small twinkle in his eye. He'd said the last bit softly, like a friend would ask about an exciting first date.

"She is" Hugo swallowed "a friend."

Luis kept his pose and soft tone. "Your coffee date, your platonic coffee date, cost us a lot of money. And the policía, they decided to burn the drugs before you left. They sent me a video." He leaned back and typed some lines on a keyboard. The video monitor behind Luis lit up and showed a brick of white powder being incinerated into black ash. The video ended after a few seconds and then resumed from the beginning. "Hugo, these supply chain issues are going to be bad. That is enough product to last the Southern California client base for three years. And here it is being incinerated because **you had to get coffee with your platonic friend**." Luis yelled out the last words, curling his lips at the word platonic as he screamed.

The door burst open and Hugo turned around. The guard was coming in with his pistol drawn and aimed at Hugo. Hugo raised his hands in the air and as he opened his mouth Luis spoke "Not now, not now. It's fine. We're just talking. As you were." he waved the guard away. "Hugo, please sit back down." Hugo realized he was in an awkward half standing pose and slowly lowered himself back into the seat. He breathed in and out until his heart-rate became normal. As he breathed Luis talked.

"I look up to other businessmen. I try to study their lessons. Bill Gates would say 'It's fine to celebrate success but it is more important to heed the lessons of failure.' and I believe there is a lot of wisdom in that." Hugo chewed on his upper lip as he looked at Luis. "You've learned a lot working for us and if we just got rid of you we'd lose those lessons and training. Honestly, I hold Frank more responsible for this than yourself. He should have taught you better. You're going to get your bakery. And you will be a part of our enterprise. The way it works will be very simple. You take paper for your pastries. You have a business license and a currency scanner so converting it into digital bank is very easy. You'll buy supplies from a company we own, say flour at ten dollars a kilo"

"That's a five times markup!" Hugo exclaimed.

"Yes, that's how we make money off of this." Luis explained patiently. "and we all profit off of this. The money we will bring in for you will let you continue to have a margin and even make a fair amount for yourself."

Hugo leaned back in his chair. "So, you'll pay for the personal certificate of good standing then?" 

Luis threw back his head and laughed. "Is that what Frank told you? He still uses that one?"

Hugo was unsure how to respond. "Yes?" he said after a moment.

"Hugo," Luis chuckled "there is no such certificate. It's a test that we use to check for credulous targets. Like those Nigerian prince emails with all the typos in them. If you'd looked around the official web site you would have failed to find anything about that."

"This is fucking bullshit!" Hugo shouted back. "I only did all of this because I thought I had no other choice. Fuck this, fuck you."

"No other choice? You didn't need to open a bakery, you wanted to." Luis smirked back at him. "The moment you transferred that first bit of money, we had you."

"You don't anymore. I want out. I'm done with this." Hugo looked Luis in the eyes as he said it.

Luis stopped smiling and typed a few commands into his computer. The monitor behind him lit up with photos. "I can't force you to work for us. But this isn't the sort of job you resign from. I want you to see what happens to people who try to leave."

There were dead bodies in every photo. Some of the bodies had bullet wounds, others were just a broken leaking mess. One person lay face down on a carpet, an axe sticking out of their back. Their hands were in front of them like they had been trying to crawl away. As he took it all in, Hugo saw a child in one of the photos, maybe 9 years old.

"We won't stop with just you. We'll go after your Felix, your Violet. Yes, I know their names. We'll even go after your Claire." Luis looked at Hugo and held eye contact as he calmly spoke.

Hugo saw a familiar face on the monitor. "Is that Paul?"

Luis glanced behind him. "Yes, that's Paul. He was always such a hothead." He turned back and gave Hugo a sad half smile. "I need to hear you tell me, are we going to be able to work together?"

There was nothing else that Hugo could say. He hung his head down and a "yes" escaped from his lips. After a moment the yes was followed by a "sir".

Luis leapt up and reached across the table. He grabbed both of Hugo's hands in his own and pumped them up and down enthusiastically. "Yes! Wonderful! I am so happy." The sadness was gone, replaced with joy and enthusiasm. Hugo looked up at Luis and saw the photos still hovering on the monitor. "When I was a young man in Columbia, I heard someone I look up to say 'All our dreams can come true, if we have the courage to pursue them'. Shortly after that I left for America and began my Black Market Peso Exchange. It was a crowded field then, but I stuck to it and adapted as the market and customers changed. I believe you can do this to. You're going to buy your parents a house, you'll even be able to buy yourself a second home in the Meta. Just get up, work hard every day, and you'll achieve every dream you've had."

"How long is this going to go on for? Is there a time when I can say I've made enough for you and enough for me?" Hugo couldn't stop looking at the dead bodies.

"I've been in this business for 38 years Hugo. As I learned from Paul Graham, 'If you can just avoid dying, you get rich'. Don't worry about stopping. Just try to avoid dying."

"Paul Graham? Is he a" Hugo wasn't sure the word to use. "A mob boss?"

"What? no. He's a venture capitalist. A very wise man who publishes his lessons for success, for free, so that the rest of us can learn from him. You should read his essays. You might learn something for your bakery." Luis released his hands from Hugo's and sat back down in his chair. A few keystrokes turned the monitor behind him off. "Go on, get going, you need to choose a location for your bakery. I want it up and running by next week." He waved Hugo away. Raising his voice he called to the door "Excuse me! This young man is ready to get going."

The door to the office was opened by the guard. His gun was still in the holster, and his hand still rested on it.

Hugo got up, and swayed a little bit as he stood. He realized his legs still felt weak. After a moment he was able to walk and he left through the office door.

# Chapter 11 - Bakery

Hugo wiped a cloth across the glass counter. Beneath the glass pastries lay on display. Puff pastries, Empanadas filled with chicken, Russian Piroshki's, and Chinese beef buns all lay on plates, with neatly labeled signs showing their prices.

The door jingled as it opened and a customer came in. "Welcome! Welcome!" Hugo looked up smiling. Frank had come in and he looked like he'd been in a car accident. His face was bruised and his left leg was in a cast. He had a crutch under his right arm and a paper bag in his right hand.

"Hugo." was all Frank said. He looked grim.

"What happened to you man? Did you get into a car accident? I tried texting you when I had my grand opening. This has been amazing. I would never have wanted things to happen this way but now that it-" Hugo stopped as Frank raised his hand.

"I'm here to pickup my special phone order." Frank set the bag down on the counter.

Hugo looked inside and saw it was stuffed with cash. "Yea, totally, I've got it right here." Reaching under the counter he pulled out and set down a large, flat, box. "Are you going to be able to manage with the crutch? I put in a little bit of everything here. Let me know what you like best. Actually, do you need me to help you take it-" Frank raised hand once again.

"No. I don't. I just want to get going."

"I hear you, it can't be easy getting around with that leg. I'm sure you're busy. I'll just ring you up and be on my-"

"Hugo, don't waste my time ringing me up. You idiot." Frank spit out the last part. "Just ring it up later. Luis put me on one of his performance improvement plans because of what you did. I'd say I hope you screw up and he does it to you, but please don't. He'll probably kill me first." Frank grabbed the box and turned it on it's side to fit it under his arm.

"Oh, no, Frank, you need to keep it level" Hugo reached out his hand. "You'll damage the pastries if they crush each other. I'm so sorry he did that to you. I'm so sorry."

Frank looked at Hugo with a manic gleam in his eyes. "Damage the pastries huh? Wouldn't want your precious pastries to be harmed would I?" He kept the box sideways and hobbled out the store. "Be seeing you around Hugo" he called over his shoulder as he left.

As he left, Frank walked to a trash can right in front of the store. He tried to shove the pastry box into the opening, but the box was too big. Frank started pushing, then punching, with his free hand to get the box in. Finally he used his elbow to slam the last bit of the crushed box through the hole of the trash can. He turned, looked at Hugo, gave him the finger, and limped off.

Hugo sat on the chair behind the counter, his mouth open in shock. "I guess I should really think about the margins for those special orders." he mused to himself. "I don't think it will matter what I put into them."
