---
title: "(🔥 Take) DJB sues the US Government, again"
description: "Then Life Continued - Hot Take"
summary: DJB sues the US Gov, Round 2
tags: ["Then Life Continued", "Hot Takes"]
date: 2022-08-05
---

The inspiration for this hot take is [here](./source)

Once again, Daniel J Bernstein, abbreviated DJB, is suing the US Government. The first time around was about declaring software protected speech under the first ammendement. This was an important case that lead to reducing controls on encryption and relaxing the hold ITAR had on exporting strong cryptography.

This time around is different. DJBs second lawsuit against the US government involves the governments failing to properly respond to a Freedom of Information Act (FOIA) request. DJB wants to better understand the relationship between the NSA and NIST when it comes to post-quantum crytography, due to the prior relationships in other forms of crypto.

A large part of the article covers the history of involvement between the NSA and NIST. It mostly appears accurate, to my understanding, and is well worth reading. The one part that I would quibble about is his tying of the OPM hack (see "If the Chinese government [stole millions of personnel records](https://en.wikipedia.org/wiki/Office_of_Personnel_Management_data_breach) from the U.S. government,\[...\]" section) to weak cryptography. As I understand it that hack was related to poor use of authentication and authorization controls, not delibertly weakened cryptography.

Overall my big conclusion from his article is that the Google / Cloudflare project to combine classical and post-quantum crypto was a good call. The combining of different algorithms was done so that even if the post-quantum algorithm had issues, the classical algorithm would prevent the message being broken. Since the algorithms were not fully vetted during the experiement in 2019, that made a lot of sense. Given the suspicisions that DJB is raising about the NSA delibertly weakening post-quamtum crypto, this technique may continue to be used far into the future.

I had long been annoyed by TLSv1.0 using both md5 and sha-1 in it's pseudo-random function since it felt like a lot of fuss to combine both. With the history of DJBs article laid out and the benefit of time it makes a lot more sense why these sort of multi-algorithm combinations are used.

Regardless of how many algorithms are combined to encrypt a single message in the future, I look forward to seeing how this lawsuit plays out.
