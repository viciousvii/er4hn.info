---
title: 🔥 Take - Analysis of Security in Popular Messaging Apps
description: Then Life Continued
summary: How to build a chat app that respects privacy and keeps its users safe.
tags:
  - Then Life Continued
  - Hot Takes
date: 2024-01-15
---

![Monsters lurking around a woman on a phone](./PhoneMonsters.jpg)
> Shadowy monsters lurk around a young woman as she attempts to safely chat with others. Knowing what messaging apps are safe to use requires a great deal of knowledge and often defaults to folkore. At the same time, the stakes are high and any slips can put users, unknowingly, at risk.

In 2023-06, Tech Policy Press put together a [survey of the security of popular messaging apps](https://www.techpolicy.press/what-is-secure-an-analysis-of-popular-messaging-apps/). The paper is around 80 pages, but it's all very readable and easy to page through.

The intent of the survey is to look at popular messaging apps and see how safe they are to use by people who may be at risk for using them. Political dissidents, pro-abortion supporters, LGBTQ+ individuals, and others whose existence or actions make them a target of others who may wish to do them harm. This is not just groups with different ideologies, or family with ill intentions. The adversary these people must be concerned about is the government which they live under. With this in mind there a high bar required for people to use these apps to communicate and remain safe from danger.

Cryptographers can only make firm statements about how secure an algorithm is after studying it. However, only a few people are cryptographers. Fortunately there are many more security engineers who can read the analysis of a cryptographer and apply that understanding how safe a chat app might be. Unfortunately the average user is not a security engineer. In some cases, Louisiana and their fight on abortions being a particular example, a user may not even be computer literate and would struggle to use moderately complex systems. In those times users turn to "security folklore" and "security nihilism" as their guiding lights for what to do. "Don't trust Meta, they want to sell you ads. Tape up your microphone when you talk about private things." the folklorist might say. "The government can just read your phone. I'm not installing Signal, I will just text you" the nihilist replies. Neither is completely true, but it's a struggle to provide education on what is best. This paper highlighted a number of the design issues that contribute to both and provides actionable advice on how to best use apps in their current state, but ultimately it will be on the end users and advocates that can educate them to make the right choices.

Tragically it is all too easy to not make the right choices in choosing what messaging apps to communicate with. As with many thing security a failure is also going to start out invisible. Then, without warning, an adversary can strike and the user can suddenly become a victim. Unencrypted backups, messages on an allies phone being seized, the government surveying messages via court order, there are many possible failure modes that can put a user at risk.

What struck me the most is that the fundamental pieces for providing that safety are already in place. The biggest impediment is perfect implementations is the desire to put it all together and to communicate that to non-technical users. It may not surprise those within tech-land to see that Signal was the best of the bunch, baring issues like requiring a phone number to sign up.

As I read through this I noted a number of key takeaways:
- Implementation of safety and privacy features is everything. Cryptography is the least hard issue: Just use a vetted implementation of the [Noise Protocol](https://noiseprotocol.org/) and call it a day.
- Where things break down is in how features are designed and used.
	- Good feature design has privacy settings on by default and requires active participation from users to turn it off.
	- Bad feature design focuses on social features and gets users to share and disseminate info widely.
	- Good feature design prevents turning off encryption and avoids footguns.
	- Bad feature design has non-encrypted fallbacks or paths where encryption is not used for communication. It's even worse when it's not clear that encryption was not used.
- It's very important to let users decide what they want to do and keep that power in their hands. Let users choose how long they want a message to remain before disappearing, let them decide if a message can be sent with encryption on or off, let them decide if URL link previews should be enabled or disabled. By letting the user decide, the user makes their own choices on risks they are willing to take.

In reading about design the paper brought up the design principles of "Privacy by Design" and "Design from the Margins". Both are highly worth reading into more. Summarizing them succinctly: Privacy by Design has feature design focused on safe default settings, openess, and only adding features that do not decrease privacy. Design from the Margins is a concept where the users most at risk, and their use cases, are treated as core parts of the design rather than an edge case with poorly thought out features tacked on the side.

Reading through this lead me to wonder: But what more could be done? What would an even better designed chat app look like?

## A Possible New Chat App

Signal does a pretty wonderful job as far as chat apps go of protecting users privacy. However there are a couple of issues that it has:
- Internet access is required to use Signal: There have been a number of protests where internet access was cut off to try and force protestors to disperse by removing their ability to coordinate.
- Possessing the app may be a crime: In some countries such as [Iran](https://www.rferl.org/a/iran-deems-signal-criminal-content-removes-from-local-app-stores/31048089.html) ,even possessing Signal is a crime. Police can stop and demand to view a phone to check for disallowed apps. It's possible that governments could subpoena app stores to learn how downloaded the app in the past as well.

Internet access being required is a problem that many other chat protocols have tried to solve. Programs such as [Briar](https://briarproject.org/), and [Scuttlebutt](https://scuttlebutt.nz/) allow for communication across alternative channels such as Wifi, Bluetooth, or even offline. It's also worth noting that the Noise protocol doesn't require a central server.

However, even Briar and Scuttlebutt require some app to be installed. It's possible to sideload an app, but this requires detectable changes to be made to a phone as well. It's possible to use a special phone just for the chat app, but that may not always be practical for logistical and financial reasons. Instead, this points towards the use of PWAs as a chat app.

Progressive Web Applications (PWAs) are a type of web site that can also function as an app. PWAs can use Wifi, Bluetooth, and storage on the phone. They can be opened as a website or saved to the phone without acting as a formal app install. This allows for a great deal of flexibility in being able to communicate peer-to-peer, install the app unobtrusively, and delete it easily.

In order for this to really work well it would likely need some equivalent of a server to receive messages. Relying on others also introduces risk, so peer-to-peer or other decentralized solutions may be the way to go. A peer-to-peer app may focus on letting others provide the servers rather than trusting a central entity. Briar does this with the Briar Mailbox. 

Delivering messages over a decentralized network is a pretty interesting problem as well. Briar will synchronize some messages to be sent by mutual contacts. Other types of messages are only sent directly. In a setting where they may be many users and no internet (ex: a crowd being forced to disperse) this sort of solution may not make the most sense. At the same time, forming a complete mesh network would have issues of it's own with regards to preventing spam or MitM attacks.

All in all, this points towards an interesting set of potential problems to explore and work on!
## Acknowledgements

Thanks to Ilia Lebedev for linking me to this in the first place.

## Image Generation

Header image was generated via Noiselith and SDXL. Details are:
```
A confused woman looks at her phone. Ominous, shadowy monsters lurk behind the user, watching her from behind. Dreamy, surreal, art style. 
Negative prompt: monsters in background
Steps: 50, Sampler: DPM++ 2M Karras, CFG scale: 30, Seed: 3265105503, Size: 1024x1024, Model hash: e6bb9ea85b, Model: SDXL 1.0 (VAE 0.9), RNG: NV
```
