---
title: "(Suggested 📚) The Cuckoo's Egg"
description: "Then Life Continued - Suggested Reading"
summary: Review of "The Cuckoo's Egg" by Clifford Stoll
tags: ["Then Life Continued", "Suggested Reading"]
date: 2022-09-16
---

## Deets
- The Cuckoo's Egg by Clifford Stoll
- ISBN: 0-385-24946-2

## Review

The Cuckoo's Egg was one of the formative novels of my childhood. Opening in Berkeley California a couple years before my birth, some accounting irregularities are discovered in the computer system. In those days users were charged for the amount of time they spent on a computer and this was initially assumed to be a bug in the accounting software. Clifford "Cliff" Stoll, astronomer in training and sysadmin to pay the bills, is asked to look into this.

What Cliff discovers is beyond what anyone could imagine. In scenes that feel too implausible if they were not real, Cliff discovers the irregularity is caused by a hacker creating an unauthorized account and using the computers as a jumping off point for stealing sensitive military information as well as accessing further military systems. Aghast, Cliff tries to find who to report this to. The FBI doesn't care, because nothing classified was taken. The CIA and NSA aren't sure who is in charge.

Finally the agencies recognize that this is a problem and something has to be done. Everyone converges on what to do as Cliff goofilly stumbles through meeting people. In one of my favorite scenes he is invited to the CIA. There he finds a set of stamps for CLASSIFIED, EYES ONLY, NO FORN, TOP SECRET, and all the other fun designations. He stamps them on some blank papers and, oh no, is caught on the way out.

Cliff proves much more adept in tracking down the hacker than he does at smuggling papers out of the CIA. Through sleuthing he is able to discover the hacker is coming from East Germany and puts together enough honeypots to track down the person's real identity. The culprit is caught in West Germany, put on trial, and Cliff later writes a book about it.

It was a fascinating story to me for a number of reasons. Growing up near Berkeley and having my own fascination with computers, it felt relatable. Cliff was someone who was passionate about learning, he lived his personal life outside of this quest, and he made everything sound so fun. As I became more interested in cybersecurity the principles of what Cliff was doing made more sense as well. Cliff would debug and understand the attack. From there during an attack Cliff would log what happened and trace backwards to discover the attackers identity. Even information such as timezones could be inferred from noting connection times. All these techniques would be refined, scaled, and automated to form the modern cybersecurity companies and technologies that exist in the world today. It was both a glimpse of the future and a story that made me believe I could be a part of it.

As an addendum I later learned that Cliff sells Klein bottles through https://www.kleinbottle.com/. In my order notes I wrote to him about how much his book meant to me. He was kind enough to write back a few messages and send some pictures of the packaging process. The Klein bottle sits on my bookshelf today as a happy memento.
