```mermaid
graph LR
	Client(Client)
	Network(Network)
	Router(Router)
	SWA(SW Load Balancer Alpha)
	SWB(SW Load Balancer Bravo)
	ServerFoo(Server Foo)
	ServerBar(Server Bar)
	ServerBaz(Server Baz)
	Client --- Network
	Network --- Router
	Router --- SWA
	Router --- SWB
	SWA --- ServerFoo
	SWA --- ServerBar
	SWA --- ServerBaz
	SWB --- ServerFoo
	SWB --- ServerBar
	SWB --- ServerBaz
```