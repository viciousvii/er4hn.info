```mermaid
graph LR
client(Client)
server(Server)
LoadBal(Load Balancer)

client -->|Physical Transport|LoadBal
LoadBal -->|Physical Transport|server
client -->|Logical communication|server
```