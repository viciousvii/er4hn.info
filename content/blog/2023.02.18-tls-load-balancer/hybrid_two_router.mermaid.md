```mermaid
graph LR
	Client(Client)
	Network(Network)
	RouterOne(Router One)
	RouterTwo(Router Two)
	SWA(SW Load Balancer Alpha)
	SWB(SW Load Balancer Bravo)
	ServerFoo(Server Foo)
	ServerBar(Server Bar)
	ServerBaz(Server Baz)
	Client --- Network
	Network --- RouterOne
	Network --- RouterTwo
	RouterOne --- SWA
	RouterOne --- SWB
	RouterTwo --- SWA
	RouterTwo --- SWB
	SWA --- ServerFoo
	SWA --- ServerBar
	SWA --- ServerBaz
	SWB --- ServerFoo
	SWB --- ServerBar
	SWB --- ServerBaz
```