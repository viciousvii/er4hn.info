```mermaid
graph LR
    Ext(Outside World)
    LoadBal(LoadBalancer)
    NodeA(Computer Node Alpha)
    NodeB(Computer Node Bravo)
    NodeC(Computer Node Charlie)
    Ext --> LoadBal
    LoadBal --> NodeA
    LoadBal --> NodeB
    LoadBal --> NodeC
```