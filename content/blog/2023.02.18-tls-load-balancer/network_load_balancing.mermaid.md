```mermaid
graph TD
	Alpha(Client Alpha)
	Bravo(Client Bravo)
	Foo(Router Foo<br>BGP advertises 10.0.0.1 and ECMP decides the server )
	One(Server One<br>IP: 192.0.2.1)
	Two(Server Two<br>IP: 192.0.2.2)
	Three(Server Three<br>IP: 192.0.2.3)
	ServiceOne(Service One<br>IP: 10.0.0.1/32)
	ServiceTwo(Service Two<br>IP: 10.0.0.1/32)
	ServiceThree(Service Three<br>IP: 10.0.0.1/32)
	Alpha --- Foo
	Bravo --- Foo
	Foo --- One
	Foo --- Two
	Foo --- Three
	One --- ServiceOne
	Two --- ServiceTwo
	Three --- ServiceThree

	linkStyle 0 stroke:yellow,stroke-width:2px;
	linkStyle 1 stroke:red,stroke-width:2px;
	linkStyle 2 stroke:yellow,stroke-width:2px;
	linkStyle 4 stroke:red,stroke-width:2px;
	linkStyle 5 stroke:yellow,stroke-width:2px;
	linkStyle 7 stroke:red,stroke-width:2px;
```