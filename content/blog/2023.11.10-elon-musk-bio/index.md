---
title: (Suggested 📚) Elon Musk
description: Then Life Continued - Suggested Reading
summary: Review of "Elon Musk" by Walter Issacson
tags:
  - Then Life Continued
  - Suggested Reading
date: 2023-11-10
---
## Deets
Elon Musk by Walter Issacson
ISBN: 978-1-9821-8130-7

## Review
This is a story which humanizes a very controversial man. Elon is someone who has a huge ego, an ability to bullshit and keep that up for years, and a reputation for running people into the ground until they've burned out. In so many ways he embodies the worst traits of the worst leaders in the tech trade. Yet, he also succeeds. He was a key part of PayPal, he brought back rockets to the US via SpaceX, he made electric cars a new category in the US rather than a tiny fraction of a niche. So much of his wealth comes from enormous, impossible bets on himself. He also embodies the best of startup culture, of hustling, of doing things by any means necessary. As Walter Issacson writes in the closing pages: "Do the audaciousness and hubris that drive him to attempt epic feats excuse his bad behavior, his callousness, his recklessness? The times he’s an asshole? The answer is no, of course not. One can admire a person’s good traits and decry the bad ones. But it’s also important to understand how the strands are woven together, sometimes tightly."

People raised in traumatic environments, by hurt people, absorb that pain and pass it on to future generations. Much of Elon's temperament, even parts of his success, is clearly the product of deep rooted pain, and I find that tragic. Seeing his ups, his downs, how all of his success brought him so little joy, helps raise him up from a boogeyman, and down from a mythological figure. His story makes him a human, a mortal like the rest of us. 

One thing I did find particularly noteworthy in his bio was how he ran projects. That was a central part of his success, at least in his own words, and it is interesting. My own commentary is in line with each statement:

> 1. Question every requirement. Each should come with the name of the person who made it. You should never accept that a requirement came from a department, such as from “the legal department” or “the safety department.” You need to know the name of the real person who made that requirement. Then you should question it, no matter how smart that person is. Requirements from smart people are the most dangerous, because people are less likely to question them. Always do so, even if the requirement came from me. Then make the requirements less dumb.

Key here is that every requirement should be questioned and have an owner. I am a big fan of attaching ownership to action items and deliverables. Once things become diffuse they lack any ownership and are never driven to completion.

Questioning requirements is important to for getting things out the door. This itself feels like a scrappy, startup mentality. Do the least you can to deliver quickly and fix later.

> 2. Delete any part or process you can. You may have to add them back later. In fact, if you do not end up adding back at least 10% of them, then you didn’t delete enough.

Though maybe it's possible to take deleting things too far 🙃

> 3. Simplify and optimize. This should come after step two. A common mistake is to simplify and optimize a part or a process that should not exist.

Make things repeatable and possible to follow. Some of the first few steps can be thought of as banging out a proof of concept and this is productizing it.

> 4. Accelerate cycle time. Every process can be speeded up. But only do this after you have followed the first three steps. In the Tesla factory, I mistakenly spent a lot of time accelerating processes that I later realized should have been deleted.

I think of this as polish in software engineering. Stabilize the product, make it more reliable, add back in missing features if you can.

> 5. Automate. That comes last. The big mistake in Nevada and at Fremont was that I began by trying to automate every step. We should have waited until all the requirements had been questioned, parts and processes deleted, and the bugs were shaken out.

The goal of all good software is to never do it a second time.

> - All technical managers must have hands-on experience. For example, managers of software teams must spend at least 20% of their time coding. Solar roof managers must spend time on the roofs doing installations. Otherwise, they are like a cavalry leader who can’t ride a horse or a general who can’t use a sword.

This is easier said than done. Reading other peoples reports of Elon many of his hands on ideas were harebrained and slowed down development. But I do believe it is important to have a deep understanding of what you are in charge of. Even if you aren't in the trenches coding, you should know how every component works and be able to understand the complete end to end flow at a technical level.

> - Comradery is dangerous. It makes it hard for people to challenge each other’s work. There is a tendency to not want to throw a colleague under the bus. That needs to be avoided.

This is ridiculous. The company is never your friend, but it is important to work together and support others. You should challenge your colleagues, but don't stab them in the back.

> - It’s OK to be wrong. Just don’t be confident and wrong.

Elon does this frequently. Step 2 of his algorithm is literally being confident, deleting too much, and needing to add it back.

> - Never ask your troops to do something you’re not willing to do.

Agreed.

> - Whenever there are problems to solve, don’t just meet with your managers. Do a skip level, where you meet with the level right below your managers.

This makes a lot of sense and ties back to needing a deep knowledge of what you are in charge of. Without that meeting with the level right below the managers will not be beneficial. I'm also reminded of the opposite of this is in "Army Leadership and the Profession", ADP 6-22, : "1-68. Direct leaders understand the mission of their higher headquarters two levels up and when applicable the tasks assigned one level down. This provides them with the context in which they perform their duties."

> - When hiring, look for people with the right attitude. Skills can be taught. Attitude changes require a brain transplant.

Absolutely. You can lead a horse to water, but you can't make it drink. You can teach someone whatever language or tool is in vogue, but you need them to have that intrinsic motivation. As the cool kids say "They got to have that dog in them."

> - A maniacal sense of urgency is our operating principle.

Not only do they got to have that dog in them, it got to be rabid. This actually ties surprisingly well to rules (1) and (2). A sense of urgency allows one to question and delete everything not essential, which in turn helps to get something out the door.

> - The only rules are the ones dictated by the laws of physics. Everything else is a recommendation.

Great for manufacturing and making physical objects. It does still kind of apply to software. You can understand something, not bother to implement it completely, and come back to improve on it later. If it is possible to do, it does not need to be done the first day.