---
title: "(Suggested 📚) The Dark Forest"
description: "Then Life Continued - Suggested Reading"
summary: Review of "The Dark Forest" by Liu Cixin.
tags: ["Then Life Continued", "Suggested Reading"]
date: 2022-08-28
---

## Deets
- The Dark Forest by Liu Cixin
- ISBN: 978-1784971595

## Review
The Dark Forest is book two of the Three Body Problem trilogy. Covering mankinds first contact with a hostile alien race, the trilogy is really about people and governments. All good sci-fi should reveal more about humans than it does about aliens with pew-pew laser guns, but The Dark Forest does a great job at this.

I was not a big fan of the Three Body Problem, the first novel in the series. I didn't understand the rave reviews, but I am glad I made it to the second novel. Upon reflection I think that I didn't enjoy the first novel because I lacked the context to understand it. The first novel opens with the Chinese Cultural Revolution. Elements of the Red Guard are turning upon each other and laying siege to those deemed insufficiently ideologically pure. Later the protagonists father is beaten to death during a struggle session after her mother gives evidence against the father. These tragedies lay the groundwork for the later events that result in hostile aliens sending out their armies to invade Earth.

If I had a better understanding of modern Chinese government and history I think that the points the first novel was making would have been more clear to me. Lacking those contexts it felt like a relatively straightforward story, with elements of Chinese life loosely interlaid on top. I found Lu's writing of characters to be wooden and lacking in an inner life as well, so the novel fell flat for me.

The Dark Forest has a concept in it that I find much more relatable: Technofetishism. The belief that the shiniest, newest, technology is sufficient in order to overcome all obstacles. I am a technology cynic and I don't trust new things. The novel laid into that theme as well over and over. What mattered most in the end is understanding others and out-thinking the enemy, a difficult task when engaging with one much superior to yourself. The ending itself made the hair stand up on the back of my head, even thinking about it today still does that. I would compare it to the reveal at the end of The Usual Suspects, in that it is hinted at over and over, but still masterfully executed in its reveal.

Liu's characters still come across as wooden and their motivations are rarely strongly expounded on. In a novel whose premise that the only place aliens cannot see is inside ones mind, the lack of clarity into the minds of the characters feels like a big miss. Overall the strength of the subject matter is what lets the novel remain strong and an enjoyable read to the end.
