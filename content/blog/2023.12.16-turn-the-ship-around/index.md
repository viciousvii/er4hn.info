---
title: (Suggested 📚) Turn the Ship Around!
description: Then Life Continued - Suggested Reading
summary: Review of "Turn The Ship Around!" by L. David Marquet
tags:
  - Then Life Continued
  - Suggested Reading
date: 2023-12-16
---
## Deets

Turn The Ship Around! by L. David Marquet
ISBN 978-1-101-62369-5

## Review

Turn This Ship Around is a surprisingly enjoyable discussion on how to transform a large, top down, bureaucracy into a more nimble and efficient one driven by people acting independently at every level. Marquet does this developing what he calls "Leader-Leader" behavior among his team. Set aboard a US Navy Nuclear Submarine during peacetime this story is one of organizational transformation among what feels like a series of low stakes situations. While a nuclear submarine sounds dangerous and exciting, the most hair-raising event that occurs is one in which the submarine nearly severs the line a tug is using to haul another boat. Which isn't to say that this book is not fascinating, just set your expectations appropriately.

The story that Marquet weaves does impress me though. I wasn't a big fan of it at first, assuming that I would only be able to apply this to a Fortune 50 company, but many of his observations and transformations felt especially relevant, even with my current job and responsibilities. Quoting from Marquet who quotes Einstein in the intro: "The significant problems we face cannot be solved at the same level of thinking we were at when we created them." What this book held for me was advice on how to scale upwards and release top down control as I am learning how to be an effective manager.

The opener of the book begins with defining the state of the Navy at the time Marquet was a captain: Leader-Follower. A top-down model which, quoting him: "developed during a period when mankind’s primary work was physical. Consequently, it’s optimized for extracting physical work from humans." His goal is to shift the submarine into a Leader-Leader mentality where each level feels individual responsibility for things under their command and they act independently to achieve organizational goals.

In order to do this Marquet made a number of changes. The following ones stood out to me in particular:
- Shifted around responsibility so that every individual is responsible for their own job. A part failing that an engineer was responsible for is a failure of the engineer, not the engineer's boss. Likewise if a project a boss was assigned to deliver on fails, it is the failure of that boss, not the bosses' boss.
	- This doesn't mean that the boss' boss is not responsible, they still are, but the boss must now bear personal responsibility as well.
- The submarine crew, famously low performing, was re-oriented away from "avoiding errors" and towards "achieving excellence". As part of that focus on achieving excellence goals and metrics were sought which would improve the performance of the submarine. Minimizing the number of errors that would occur was a natural outcome of those positive goals.
- Empowered the lower levels to provide their own thoughts and self-organize solutions. This was called "Don't move information to authority, move authority to the information" and would avoid a slow back and forth decision making process.
	- Interestingly enough this crops up in other places as well. "Hayek's knowledge problem" describes the issues with centralized planning. While his article was focused on the economy it applies to a great many other places.
	- This would often be expressed by having someone inform their superior "I intend to do X Y Z with the following reasoning" at the appropriate time rather than wait for orders to trickle down.
	- Team members could question each other's reasoning and discover errors with this mechanism as well.
- Made sure that projects were checked in on early on to see how they're doing. These check-ins were kept short, and the work was understood to be rough. But this kept projects from being detailed or being marked as "not acceptable" when they deviated from the requirements the higher levels had thought was needed.
- When a new project or solution to a problem was needed, don't inform others what that will be. Instead, explain the problem. Involve the team to get their solutions before making a decision.
	- "Specify Goals, not Methods" would come up during this process.
- Pushed technical competence down through the levels. As each person's authority increased they needed to understand more of what they were doing in order to make the appropriate judgement calls and reactions without someone higher up needing to tell them to do so. 
	- This would take the form of needing to not only understand their area of responsibility, but how what they did tied into other parts of the ship. This was more than just technical systems: this would involve preparations for emergencies, improving paperwork driven processes, and many other places.
	- This also led to a change in how people were taught that they called "certification." In essence, training was low quality and done frequently by having someone lecture you. You'd forget most of the lecture, muck up a bunch of drills, and learn through more training and more drills. Certification had the leader quiz team members prior to the exercise occurring, potentially failing everyone in advance of the exercise if there seemed to be a lack of required knowledge. This both made the exercise more active and encouraged each team member to be aware of what they should be doing.

The book goes into a great many more details and provides specific guidance on how to achieve a Leader-Leader model. In addition, a point I liked, each chapter ends with a set of questions for self-reflection and self-improvement.

This was a fairly short read and proved to be quite useful. I would highly recommend this to anyone interested in building their team up to be independent leaders.