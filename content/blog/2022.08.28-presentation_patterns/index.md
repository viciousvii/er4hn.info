---
title: "(Suggested 📚) Presentation Patterns"
description: "Then Life Continued - Suggested Reading"
summary: Review of "Presentation Patterns" by Neal Ford, Matthew McCullough, and Nate Schutta.
tags: ["Then Life Continued", "Suggested Reading"]
date: 2022-08-28
---

## Deets
- Presentation Patterns by Neal Ford, Matthew McCullough, and Nate Schutta
- ISBN: 978-0321820808

## Review

Presentation Patterns is a book about, 🥁, crafting better presentations. Presentations consist of a topic, a set of items to discuss, and ideally some sort of visual slide based elements to go along with it.

The "patterns" the book refers to are formats a presentation may take. There are good patterns such as "Fourthought" or "Context Keeper". There are bad patterns such as the "Bullet Riddled Corpse". The names are distinctive and each pattern contains information on how to best use its strengths or how to get out of being trapped in a bad pattern. Reading the book once tells you about the different patterns and lets you pattern match your own work as you build out slides.

The starting parts of the book on how to plan out a presentation are what I found the most useful. They are foundational, as a well planned out presentation is going to be much more important than any slides or ad-libbed talks that follow. Presentation Patterns goes into detail on all the elements required to have a smooth sounding and persuasive presentation. 

The patterns themselves can feel hit or miss. The names are cute, and it is great to recognize if you are building the dreaded "slideument" to pass around. The best use of the book after it is read is to have a physical copy. If you feel stuck on how to lay out content you can flip through the various patterns to see something that looks good. You'll see the  (bad) anti-patterns as well and be able to course correct yourself in time.

### Takeaways

To help others decide if the full book is worth their time, here is my overall summary of the book contents:

Before doing a talk, research the audience. Learn the expected attendees and their expected skill level, attitude, knowledge, community, and shared vocabulary.

Prepare to engage with the audience for the presentation. A good slide deck is only one part of the means of engagement. It may be the least important part of the engagement as well. A good presentation should leave the audience feeling like they learned something new.

Before beginning a presentation go through a few stages:
* Ideate - figure out what you want the overall theme and topics to be.
* Capture - lay out those concepts and items in a way that makes sense
* Organize - figure out the best outline to show those ideas
* Design - actually begin putting the presentation into a slide deck

After that, be prepared for multiple rehearsals. A talk may have to be done 4 times in advance before it is ready. During those four iterations you should focus on a specific set of items:

1) Find out how the talk sounds, pace the content, pace the timing, and figure out problems.
2) Focus on the presentation. Look for delivery and appropriateness of content.
3) Focus on pacing more and check that the presentation seems like it is in a good place. Do some fine tuning.
4) The rehearsal before the real thing.

A good rule of thumb is you will spend one hour preparing for each minute of presentation.

Plan on 10 - 20 minutes of material at a time before needing to give the audience a "break." The break comes in the form of a joke, a story related to the topic, getting audience members to participate, etc.

A good presentation should have 3 topics or major themes. If you are building up a presentation as a narrative arc, this can be 3 acts, with the final one providing the key takeaways that resolve the tension of the first two.

Avoid live demos except under specific conditions (listed below). Aim for pictures, or pre-recorded video if at all possible.

When discussing topics the audience may not know about, give a brief (1 minute) overview of it before going into the detail. Try to research beforehand the audiences skill level.

Plan to arrive early and "warm up" the audience with light conversation to get them prepared to listen to you.

Always act as a leader to your audience. You are there to make effective use of their time and educate them. Do not sell yourself short or focus on problems. Make effective use of time and focus on providing value to them.
