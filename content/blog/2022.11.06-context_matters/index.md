---
title: "Context Matters"
description: "Then Life Continued"
summary: Context matters, but can be lost with time
tags: ["Then Life Continued"]
date: 2022-11-06
---

"Nice shot, **nimrod**!" A silly rabbit named Bugs Bunny taunts a chubby, bald, hunter.

"ooh, you wascaly wabbit. I'll get you next time!" Elmer Fudd retaliates and waves his gun about in frustration.

Schoolchildren around the United States watch and use "nimrod" afterwards as an insult. A nimrod is someone incompetent, an idiot who consistently gets basic things wrong. That's what Elmer Fudd always does, so that's what the children learn. A few pay attention during Bible study later and learn that "Nimrod" was a mighty hunter and king. Those children learn what irony is.

Over time some actions become detached from their original context within a broader culture. They become a scene, which floats adrift and takes on a life of its own. These scenes develop their own interpretations and meanings. Below are a few of the ones I've noticed. Feel free to write in with more.

## Oedipus

A man kills his father and has sex with his mother. A classical Greek play, many people have heard of "Oedipus", the protagonist of "Oedipus Rex". Sigmund Freud comes up with the term "Oedipus Complex" to describe the psychological desire of a male child to do the same as Oedipus, citing jealousy. People use "mother f\*cker" as an insult when taunting one another in the schoolyard. It becomes a part of the cultural milieu, at least in America.

Yet, that is not what the original play was about. Oedipus never desires to kill his father nor marry his mother. He was adopted, in secret, as a baby and learned of the prophecy surrounding him as an adult. He fled from his adoptive parents, thinking he would bring harm to them and having no desire to do so. The original play was about how one cannot avoid their fate. At the end Oedipus is horrified by his actions, stabbing his own eyes out rather than witness the horrors he has wrought.

## The "No Cuts" Sword

Princess Mononoke,  a popular Japanese anime movie by Studio Ghibli, is being prepared for American release. Voice actors are being lined up to dub the characters words into English. Marketing and promotions are being decided on for American audiences. Hayao Miyazaki, the notoriously perfectionist director behind the movie sends a gift to the studio preparing the movie. It is a katana and a simple note is attached: "no cuts".

Ultimately the movie is released with no cuts made to any of the scenes. The movies is a success and fans debate the meaning of the sword and note afterwards. Was it a threat should changes be made? A request with a thoughtful gift for a wall? People opine on how the director of Studio Ghibli must be very controlling to just send something like that out of the blue. The sword and note become a story passed around message boards, a tale about the importance of controlling how others express your vision.

The sending of the sword capped off a disappointing moment almost as long as the history of the studio. "Ghibli", an Italian word, refers to a dry dessert breeze. This is also where the name of the entry level Maserati cars comes from. Studio Ghibli was founded, and named, after their first successful movie "Nausicaä of the Valley of the Wind". The movie was recut and heavily edited for American audiences as "Warriors of the Wind". The editing job was so bad that the recut movie lost its main protagonist, it's themes, and didn't even have voice actors that understood the plot. From that point Studio Ghibli refused to allow any edits to be made to their movies. So when an American producer (the infamous Harvey Weinstein) suggested cutting a few parts of Princess Mononoke, he got a sword and a note: "no cuts".

## How many times Japan comes up in futuristic dystopian Sci-Fi

Cyberpunk. The name of the genre evokes all sorts of imagery: powerful corporations, internet metaverses, neon lights, rogues unable to work a steady job and who live on the edge of the law. Then there is the kanji everywhere. The rise of Japan as a world superpower. The dominance of Japan holds true across so many games and shows: Shadowrun and all the best technology coming from Japan, Weyland-Yutani corp in Aliens, and the imagery of Blade Runner. Even in 2020 Japanese corporations play the  antagonists in Cyberpunk 2077. But why is Japan, of all places, such a success story in dystopian fiction?

The 80's in the United States was a very different time from today. Ignoring the cocaine and disco, the economic and political elements of the world felt like cyberpunk. Regan was busy deregulating industries and granting more power to corporations. Japanese companies were doing amazingly well too, to the point that Rockefeller center in New York City was bought, outright, for $1.4 billion by a Japanese real estate company. The success of the Toyota Camry, Sony Walkman, Hitachi... massage wand, and Toshiba semiconductors had American corporations on the defensive, cargo culting every Japanese business practice that they could. Into those times swept William Gibson with Neuromancer.

Gibson's novel was what defined the cyberpunk genre, and it wasn't about a world removed from our own. It was a look at what many thought would be the future. People at the time did think that computers were going to be virtual fully immersive experiences. When the man in the Oval Office is saying he wants to deregulate and shrink government wherever he can, it seemed natural that corporations would surpass the government in power. And during Japan's "miracle" period of economics, the Imperial Palace in Tokyo was worth more than all the combined real estate in California.

Time moved forwards and with it a lot of things changed. Japan's economic miracle turned out to be a bubble. The balance of power between governments and corporations swung back towards regulation. Neuromancer, however, remained a classic of literature and created the cyberpunk genre. It influenced many other works around it, and the symbology those works spawned remained with us over time.

## Tilting at Windmills

Don Quixote is often considered the first "modern novel". It's main character created the use of the word "quixotic" when he tries to charge down a windmill, claiming it is a giant and is oppressing the people. He is told it is just a building, yet he charges at it, and is knocked by a passing vane of the windmill onto the ground. His actions become a common phrase meaning "to use time and energy to attack an enemy or problem that is not real or important." (cited from Merriam-Webster dictionary).

When Miguel de Cervantes wrote the plot, the early 1600s were a very different time from today. So much has changed that a reference one may not pick up today would have been very obvious to a reader from those days. In the year 2022, windmills are used to generate electricity. In the 1600s windmills were used to grind grain.

The people who operated the windmills were called "millers" and they were an important profession at the time. Grinding grain was a tedious process, involving hours of hard effort to do by hand. A miller with his (yes, this was the 1600s) windmill would be able to quickly grind the grain. Grinding grain was essential for baking bread, which made the miller a key part of any town. It also made for a convenient chokepoint to collect taxes and fees. Millers would weigh the sacks of grain and take a portion for themselves. The lords in charge of the region would also collect a tax from the miller. Millers gained a reputation for either being patsies of avaricious lords or dishonest thieves themselves, in either case taking larger portions than was warranted for their services.

Looking upon the windmills with this context, Don Quixote's words suddenly seem less like ramblings of a confused man, and more of a criticism about the role the windmills played in society. Even his attempt to challenge the windmill and being knocked flat on his back takes on a deeper meaning. Personally, I am reminded of cloud computing services.
