---
title: "(Suggested 📚) Design of Everyday Things"
description: "Then Life Continued - Suggested Reading"
summary: Review of "Design of Everyday Things" by Donald A. Norman.
tags: ["Then Life Continued", "Suggested Reading"]
date: 2023-04-02
---

## Deets
- Design of Everyday Things: Revised and Expanded Edition by Donald A. Norman.
- ASIN: B00E257T6C

## Review

Design of Everyday Things is a book that feels timeless, in technological terms. It hasn't endured as long as Marcus Aurelius' Meditations, but it was published in 1988 and few books from that era are still relevant to a software engineer today. In his book Don discusses Design: What makes it functional, useful, and easy to use.

Like many books that attempt to predict the future, Don got a number of things wrong. Very few of his postulations about where design could and should go felt like they came true. Where I found the enormous value in his book was from his thoughts on good design. Good design is something which remains timeless and is not a prediction of the future. By trying to follow principles of good design, even someone who is not a designer can make useful APIs, write good documentation, and design useful features.

Much of the book is focused on the concept of "Human Centered Design" (HCD) which states that one needs to put human needs, capabilities, and behaviors first. A thesis of the book is that designs should accommodate those requirements. One of the most important parts of a good design is clear communication when something goes wrong and the user enters an error state. Don puts this well with a powerful quote: 

> It is the duty of machines and those who design them to understand people. It is not our duty to understand the arbitrary, meaningless dictates of machines.

Two powerful concepts stood out for me as I read through the book: conceptual models, and knowledge of an object.

The phrase "conceptual model" entered my mind and kept on rolling off my tongue in later weeks as it would come up in my work. A conceptual model is a simplified explanation of how something works. This could be a product, such as a car. This could be a process, such as how to drive that car from your house to the grocery store and park at the store. Conceptual models are the primary means by which people understand how to use something and are inferred from the device rather than reading manuals or asking experts. It is therefore important to make sure that the _correct_ conceptual model is made clear to the user so that they do not end up with an erroneous understanding.

Knowledge, and how it exists "in the mind" vs "in the world" was a fantastic concept to see elucidated as well. Knowledge of how something works is used in deciding what to do to accomplish a goal. Quoting Don:

> Perfect behavior results if the combined knowledge in the head and in the world is sufficient to distinguish an appropriate choice from all others

We all have some level of knowledge in the mind when we interact with something. This is based on prior experiences, training, and similar objects. Similarities are tied to conceptual models and are why buttons in software UIs look like buttons in physical objects: so it is obvious you can press them to do something. It's also why software UIs, across products and from competitors, will use similar symbols for saving data, altering fonts, and will often adopt similar layouts for displaying data.

With specialized tools knowledge in the mind is not always going to be sufficient to unambiguously distinguish the appropriate choice from all others when a specific goal needs to be achieved. That is where knowledge in the world becomes important. Knowledge in the world covers documentation, examples, and clear feedback from a product when the user appears to be using it incorrectly. Knowledge in the world can often be more important than that in the mind because it can be transferred to others to become knowledge in their minds. Tools which offer numerous different actions or are rarely used also benefit from knowledge in the world because a user cannot reasonably be expected to keep in their mind something they are not using.

The book goes into much more than the above, I simply listed my favorite items. It covers the theory of how one interacts with objects and divides that into affordances, signifiers, constraints, and feedback. The different stages of feedback and a user understanding how to interact with something is covered as well. Later on different examples of special cases with controls, preventing undesired actions, and how to guide users to appropriate behavior is discussed.

The book closes in the end with a discussion of design changes and overall thoughts on how design can fit into creating new products. I found this to be the weakest part of the book. I suspect, but did not check, that some of these parts are part of the expanded 2nd printing of the book. Many of the thoughts the author had on the future of design did not feel accurate and by the end the author seemed to be wildly waving their hands as they postulated on the future.

Overall I would consider this book a valuable read. By learning what is in it, even a software engineer can understand how to make what they design more usable and useful for everyone else around them.

