---
title: "(Suggested 📚) Becoming a Technical Leader"
description: "Then Life Continued - Suggested Reading"
summary: Review of "Becoming a Technical Leader" by Gerald Weinberg.
tags: ["Then Life Continued", "Suggested Reading"]
date: 2023-08-12
---

## Deets
- "Becoming a Technical Leader" by Gerald Weinberg.
- ISBN: 0-932633-02-1
## Review
Technical leadership is different from more regimented forms of leadership because technical workers are knowledge workers. A worker in the tech industry can be individualistic, with some sporting strong egos and low EQ. They are opinionated and stubborn. If you can figure out how to manage them in groups, they can accomplish absolutely amazing things.

To be an effective technical leader one has to master switching between multiple levels of leadership. With your own team, you have to lean into the details and understand each individual as a richly detailed and complex human being. Each person you work with individually has their own thoughts, goals, and motivations. This is called "organic" leadership in the book. Zooming out, when you work with larger groups and see your teams place in the organization you need to apply structure and rigor. Repeatable processes and clear guidelines work best at a higher level. The book referred to this concept as "linear" leadership.

To be an effective technical leader to your team you need to provide them with "MOI". MOI is an acronym which stands for:
- Motivation: Helping others feel inspired to do their work.
- Organization: Creating an environment in which the team can collaborate in order to do their work.
- Inspiration: Providing the space where technical workers can explore solutions to problems and come up with the best solution for a problem.

Many of the chapters in the book explore how to do this from different perspectives and with different goals. The stories the author uses feel a bit dated, but he closes every chapter with a series of introspective questions for you, the reader, to ask yourself. I greatly enjoyed those questions because they forced me to think about myself more and seek ways to improve myself. It also slowed down my reading to one chapter per week, which I felt was great to give myself time to absorb the contents.

The topics above are tied together with interludes describing where power as a leader comes from and how to become a leader. The word "manager" is not used, with deliberate intent, because this is not a book for managers. Chapter 14: "Where Power Comes From" discusses this in great detail. The following is not a quote from the book, but an excerpt from my notes on the chapter:

> Power, as a leader, is a result of your ability to influence others. It is not granted by muttering a secret incantation, nor bestowed ritually upon becoming a "manager".
> 
> Power can come from relationships in which people behave as though oriented around a leader....

I felt like this was an empowering thing to realize, because throughout my career I have had to try and be a leader, while rarely operating as a manager or someone placed in a position of organizational power. Seeing that reflected in the chapters of this book made me feel like that was okay and that I didn't need to be in the right place in an org chart to be a leader.

Closing out this review, I'd recommend this book to anyone working in a technical field. Even to those who aspire to be strong individual contributors, it is very hard to escape being a leader. No book is perfect and filled with everything one needs without any junk, but this book has a very high signal to noise ratio.