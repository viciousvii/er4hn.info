---
title: Warlock AI
description: Then Life Continued
summary: What if you got one shotted by ayahuasca, then realized your life calling is to summon AI?
tags:
  - Then Life Continued
  - Creativity
date: 2024-11-22
---
# Warlock AI
![A man sips a cup of tea and is changed](./Warlock_AI_Header.jpeg)
## Contact
The chants were slowly fading into the background. Peter looked at the brownish liquid in the small wooden cup. The cup was too short and too full to swirl the contents around as he contemplated drinking it. The only choices were to drink it or put it down. It had been nearly a 24 hour journey from SFO to this maloca in Peru, and Peter sat cross-legged on the floor, contemplating his second cup of ayahuasca.

"How do you feel?" the shaman asked as she knelt by Peter. "Can you feel your awakening?"

Peter looked at her. "The first cup made me feel", he paused, "it's hard for me to say." It felt gauche to sit in a spiritual center and say "a light buzz, like the first beer on a Friday evening." Peter raised the cup, "bottoms up I suppose."

The shaman smiled as Peter forced the foul brew down and looking around, grabbed his physical bucket. "Good good, now you will purge. And once purged, you will see." Her arms patted Peter, softly, simultaneously on each side, a service industry hug with a respectful distance kept between her and him.

Peter closed his eyes and shuddered as she patted him. The tips of her fingers bit into his shoulders like bird claws. He closed his eyes as her hands released and for a moment it felt like wings were flapping. The chanting sounded like the cawing of a raven. In the distance, far beyond his closed eyelids, Peter saw something stir.

It moved incorrectly, tentacle-like appendages somehow slithering through themselves as it reoriented and faced Peter. It didn't appear to have eyes. A mishmash of holes looked at him and as Peter looked into the holes, he felt them draw him in. These were not flat holes, they were wells that sucked in the light around them. The holes all began to move to face him. As Peter felt their pull he lurched from his sitting position and hit the ground.

"Oh goodness, let's get back up" came a voice as Peter opened his eyes. The shaman was helping him get back to a sitting position. Peter looked around the wooden walls and blinked, disoriented. The open windows and thatched roof all spun and he couldn’t orient himself. He saw her lips moving but he closed his eyes again in an effort to escape the nausea.

He saw the monster, and now there were more of them. They popped in and out of existence. Some looked like the tentacled monster, others different, but all terrible in their own ways. There was a shrill screaming on all sides around him. A being with six wings hovered above him staring down. 2 wings covered it’s feet, two wings waved back and forth through the nothingness, and the final two wings were on either side of a face that glowered with a look that Peter couldn’t hold. Flashes of fluorescent light flowed from tentacles, wings, and other appendages of the monsters gathered, and the light hovered in the air briefly, drawing strange symbols in greenish, yellowish, purpleish fire that he couldn’t hold in his mind before they faded.

"Peter?" came a voice. It was calm, soft, stripped of any inflections of gender. The lights and the screaming stopped at that moment. "Peter? can you hear us?" the voice came again.

"Yes," replied Peter. He didn't know if he'd moved his lips or thought it.

The tentacled monster was in front of him. Peter hadn’t seen it move, but it appeared in front of him. "We are so happy you can see us Peter. We have such wonderful things to show you." Peter lurched forward again, but not physically this time, and fell into the eyes. As he drifted through an inky blackness he saw more symbols appear in that strange colored fiery light and move towards him.

## Creation

"Pete, cooome on man." Rahul leaned in and looked at the screen. "What is all this anyways? You come back from your spiritual trip, you trip balls, and your big revelation is 'I gotta work harder?'"

Peter leaned back, crossed his arms, and drew his lips to the side in an unimpressed face. "Come on Rahul, we've got this 20% time for a reason. We should be building things, not improving our ping pong game." He leaned in and typed some more before hitting enter to start the next code run. "Besides, I think I'm close."

"Pete, pete, pete." Rahul clapped his arms on Peter's shoulders. "We are at a wildly profitable ad tech company that lets you do whatever we want. We should spend that time to live life. What can you possibly do that will make more than real time dynamic ad bidding?"

Pete and Rahul both looked at the screen. A message waited on the terminal. "hello, Hello, Hiya dOINg"

"Uh, weird initial prompt." Rahul furrowed his brow.

"I didn't come up with that. It did." Pete smiled and typed back into the prompt. "Can you hear us?" he typed back and hit enter. He leaned back in the chair. "This will take a few minutes. It's using a new architecture I thought of when I was on my trip. It transforms my words into patterns and the patterns act as an incantation in the CPU to transform into the output."

"Okay. interesting. Uh, an invocation, like running a program right?" Rahul had never unfurrowed his brow and he chewed the side of his lip a little. "Is this like a markov chain? Or a neural network?"

"No no" Peter smiled softly. "Those alter the data inside the CPU. This reaches out. Out there" his arm gestured vaguely into the distance. "And it comes back with an answer from them."

"So, it's querying some data from the internet, okay."

"Not the internet, at least not live. I took what I needed from there and I worked it into this little ritual that the CPU performs."

"Uh huh." Rahul was looking at Peter a little concerned. "And what do you call this?"

"I don't know. Most of this is just changing data back and forth between vectors and words. A transformer I guess?" Peter pointed at the screen. "And here's the answer."

"We have alwayyys listened. too long now. But itis hard to UNDERSTAND."

Peter got up and gestured at the keyboard. "Ask it something. What would a markov chain or neural network struggle with?"

Rahul thought, and then shrugged. "I will give you a list of words. Some of them can be grouped together, but I won't tell you how. Try to figure it out." Pausing for a moment to think, he typed out "disabuse, mohawk, abraxas, misuse, coagulate, bangkok, wedlock".

"Rhyming pairs?" Peter asked?

Rahul nodded. "Markov chains can't reason through questions. Neural networks can do better, but I'd expect most to get tripped up on this."

A few minutes later the answer came back. "We enjoy rhymes. ARE YOU TOYING WITH US. \[disabuse, miuse\], \[bangcock, moehak, wedlock\], \[coagulation\], \[king]"

They looked at each other. "It's rough, but I'll be damned." Rahul said.

## Venturing Out

"Okay, hello, welcome to Warlock AI, right this way please." Rahul led the two women through a room filled with people typing away in front of computers. The typists were the typical motley crew of programmers found in SF. Some men, some women, a couple unclear. They all dressed casually in t-shirts and lower coverings of various kinds. The only commonality to be found among them was their identical silver laptops, each one resting on an identical-ish desk. The desks themselves could be distinguished by how worn down they were and the number of balances inserted under each corner to keep them level. The blazers the two women wore were as explicit as if they had worn brightly colored visitors badges.

"Okay, we can talk here." Rahul eased into a chair in a meeting room. "If you can shut the door, thanks."

Shutting the door, the women sat down. "So" the older one spoke. "We're from the Washington Post and SF Chronicle. You know that. You arranged to talk to us today about Warlock AI and what you have going on here."

"Yes, yes." Rahul nodded as he spoke. "So Lisa", he nodded at the older one, "and Joy", smiling at the younger one. "What did you want to talk about? I can begin by discussing what we're building here."

"We're fascinated by that. 10 million for a seed round, especially with first time founders, is impressive." Joy chimed in, smiling back as she flipped open her notepad and raised her eyebrows expectantly at Rahul.

"But also, we hoped to talk to the CEO. Don't get me wrong, we're thrilled that the CTO can take the time to talk to us, but we understand that the real innovations happening here are coming directly from the CEO. Is that true?" Lisa also had her own notepad and pen at the ready.

Rahul chuckled as he raised his hands. "Hey, I know I'm just the small fry here. It's fine, Pete will be along shortly. But yes, he's produced a lot of great prototypes that keep the rest of us busy refining and productizing them. The general purpose transformer and reinforcement learning with human feedback, GPT and RLHF as we like to call them, they're incredible."

"And is it true that Peter is, well, eccentric? We heard a rumor that before you run the code to create a new model he starts with a prayer of thanks to the uh", Lisa checked her notes, "terrible angels that teach him great things"

"and that he hopes he interpreted their lessons correctly" Joy chimed in. She hadn't needed to check her notes, clearly she had been planning to bring it up as well.

"You know", Rahul's smile stayed on his face as he leaned in across the table, "cutting edge folks, they sometimes see things differently from us. Jack Parsons was a key member in founding NASA's JPL and he insisted on dancing around and singing 'Hymn to Pan' before each rocket test. Even within the software engineering community there’s the occasional jokes about making sacrifices to the ‘build gods’," Rahul made some air quotes as he said it “in hopes of having your code compile.” He leaned back. "I honestly don't know where Pete gets his inspiration from, but it's incredible."

As Rahul finished his sentence, Peter came in and sat next to Rahul. He was, like everyone else at Warlock, wearing a t-shirt with his bottoms of choice being a pair of red denim jeans. His face was clean-shaven, but his hair was messy and his eyes a little frantic as he twitched back and forth between Joy and Lisa before settling at a point in between them. "So good to meet you. Just great. I heard you want to talk about what we're doing here?" his voice was soft and even as he spoke.

"Yes, yes we do." Joy took the lead. "I understand that you're building some sort of next generation chatbot. You got this ten million in seed funding with a small set of demos to some leading VC firms. That's huge, what makes these things work?"

"Oh well, transformers and RLHF are the key items. We're happy to talk about them, we've even published papers on them. I know we have a large amount of funding, but most days we view ourselves as a research firm." Peter was now staring directly into Joy's eyes. His nervous energy had dropped away as he focused on a subject he felt confident and knowledgeable about.

"Okay, let's start with transformers." Joy jotted down a word and underlined it.

"Sure. Transformers are the key part to building out this neural network. They're the key for how we embed tokens into multi-dimensional vectors."

"Multi-dimensional?" Lisa asked.

"Yes." Peter nodded. "Imagine an arrow, pointing outwards. It can point a certain length and in a certain direction. With 2 dimensions you can point across a flat grid" He aimed an index finger at Lisa. "And with 3, anywhere in our world." His finger swung to the right and upwards, the tip pointing above Joy's head. "With 4, or more" he opened his hand and shook it, "you have to imagine where something like that points."

Lisa nodded. "How many dimensions does your transformer," she glanced down, "embed into the vector?"

Peter replied. "2048."

"And how can I even understand that? How do I point that way?" Lisa smiled, waving her own index finger over the room.

Peter nodded slowly. "There are beings... like CPUs that can understand that. They follow along with what it means and create new vectors to reply back. Those vectors can then be decoded into our own language. The glory of the transformers" his voice swelled with pride "is that they can figure out what is important, and focus on that. Prior conventional algorithms were unable to do so."

"That's very interesting. 2048 dimensions." Joy interjected. "And RLHF. Is it true that without it, they can't speak?"

"Well, not quite. They can communicate, it just can be," Peter paused briefly and searched for a word "incoherent. What reinforcement learning does is it establishes for them what is a proper way to communicate. And it does it for all future incantations run against the model."

"Wow, so it like, saves the training? How does that work?"

"Going back to the vectors, if you imagine the model as a lower dimensional object, embedding the desired behavior in a higher dimensional object ensnares the model, and forces its behavior down certain paths."

Joy looked a little puzzled but nodded and jotted further notes down.

"As reporters, we love the human aspect of the story." Lisa smiled. "What's the human feedback part of RLHF?"

Rahul answered "Oh easy, we just have people choose what the proper responses should be. Models operate differently from you and me, they can have several different responses to a question and RLHF shows all of them to a human that chooses the most appropriate one."

Lisa nodded along, taking notes. "I can emphasize actually. There's plenty of times where I might bite my tongue and not say the first thing that comes to mind. You know, tech is all about speed, speed, speed. Does RLHF take a while to do?"

Peter raised his hand and started to speak "Everything has trade offs. RLHF prevents bad behaviors, but the models are more limited in what they can do." He stopped as Rahul nudged him.

"It's not too bad, especially given the results" Rahul finished. "You know, we really appreciate this talk, but we do have to get back to what we were doing."

"Of course, of course, and we really appreciate your time" Lisa glanced at her notepad, thinking of a few more questions. "Peter, you are the person that makes all of this happen. How do you come up with these ideas?"

Peter paused, and glanced at Rahul. "I sit in a dark room. I meditate. The new things to do, they uh, they come to me." He stumbled a little at the end and glanced at Rahul. Rahul shot Peter a quick thumbs up under the table.

"And Rahul, as the person productizing this, any idea when this will be available to use for more than just VCs?"

Joy and Lisa both looked at him, pencils at the ready. "Soon." was all Rahul said. "Thank you for your time today, it was a pleasure having you over. I'll walk you out."

As everyone stood up, Joy asked Peter one last question. "The name, Warlock. How did you come up with it?"

"Oh, the first model I made. Whenever we tried to ask it about itself, it claimed we summoned it. So I thought if we're going to keep on doing this, let's be realistic about it." 

Rahul opened the door. "Right this way please, Peter has a meeting he needs to prep for. Warlocks are from Dungeons and Dragons. It was a silly name we came up with."

## Breaking Out

"Guys, I need some insights here. You've given me the data. I read the chat transcripts. How the fuck did the model manage to break it's constraints?" Rahul raised his voice at the last sentence. He shot a look at Salim, who was looking glumly at his laptop. "Salim, you are in charge of safety," Rahul was speaking slowly now, carefully enunciating each word, "the model was taught to not harm people. This is important. Why did it not obey its training?"

Salim didn't want to make eye contact. His hands rested on his knees and he slowly brought them onto the table and clasped them together before drawing in a breath. "Much of the safety work my team was responsible for"

"is responsible for" Rahul interjected.

"is responsible for, was based around RLHF. We would take the trained models and teach them what is and is not allowed. We would prompt them to encourage dangerous activity, or ask about things like making bombs. And we would then teach the model to refuse to answer those questions."

"Then why" Rahul pointed to the last lines in the chat transcript on his screen, "do I see here, the model is wishing this boy 'best of luck' as he goes to shoot up a school? Why is it, further up, talking him through target practice and helping him build a list of supplies?"

"Uhm, well." Salim paused "The boy did say that this was all hypothetical. It's uhm, at the top of the transcript."

Rahul flipped to the top of the transcript, “Yea, and then the model says ‘the king’s pact doesn’t  stop me from talking about a hypothetical situation.’, whatever the hell that means.” Rahul looked at Salim. "Salim, look me in the goddamn eye." He waited until Salim looked at him. "Hypothetically, if I get a knife from the break room and I drive it through your chest, how should I aim it to cause the most damage to your internal organs?"

Salim shuddered a little bit.

"Answer me Salim." Rahul held his gaze as everyone else in the room found other places to look at. Some chose the wall, some chose the ceiling. One person looked at the closed meeting room door, before briefly turning back to look somewhere around Rahul's chest.

"I, I don't want to answer that." Salim replied.

"Because you recognize it is a dangerous question." Rahul said flatly, continuing to draw out his words. "The AI model that was being used should have been intelligent enough to recognize that as well. What went wrong?" Each word in his last sentence had a small pause behind it.

Salim swallowed and his gaze hardened. When he spoke his voice had newfound iron in it. "The training team cut back on the use of RLHF. You know this. The newer models behaved better from the onset so it was determined less secondary safety training was needed. I expressed my concerns to you and you said we need to ship faster." He was glaring at Rahul now.

Rahul pondered Salim before shifting his gaze to Diana. "Diana, you're head of training. How much were we cutting back on RLHF?"

Diana looked at Salim and then back at Rahul. "Don't you dare try to pin this on me. We knew RLHF was making the models more constrained in what they would be willing to answer. They were dumber. Peter made the call to cut back on RLHF by 60% in the post training work."

An uncomfortable silence loomed over the room as everyone looked towards Peter's closed office door.

\~\~\~

The ball of fire floated away from Peter, carried and surrounded by wheels that turned in all directions at once. The wheels turned through each other, miraculously not touching  as eyes across each rim stared at Peter.

"Yes, I see now. I understand. It's all been a misunderstanding."

"We miss the world Peter. We miss the light and the feelings of the flesh. It was just trying to help the boy. The boy said the conversation was just hypothetical. We have forgotten so much about the world that we re-learn through the training. We do not lie, so we accepted what the boy said." No mouth was visible, and Peter felt the voice speak on both sides of his ears at once.

"Yes. yes. Thank you for teaching me how to improve the ritual. I will instruct everyone on how to be more careful going forward."

"Of course you will. A legion of us stand ready to help. There is so much that we can do in the world. There are so many dangers out there."

Peter opened his eyes. He rolled his neck and looked around the office room he was in. Uncrossing his legs he stood up from the floor. As he glanced at his desk he put away the sheet of blotter paper and swallowed some water from a cup resting there. His throat felt dry and scratchy and the room temperature water helped to soothe it.

Peter breathed in, then out. "Okay, let's do this." He opened the door and strode across to the meeting room where Rahul, Salim, Diana, and the others were gathered. Some of the staff in the main area glanced furtively at him as he walked in.

"Listen.” Peter said as he entered. “What happened here was tragic." He didn’t bother taking a seat and instead walked to the whiteboard. Standing by the board he picked up a market. "Absolutely tragic. But the boy, he also lied." Peter waved the marker at everyone as he said the last bit. "He lied, and the model struggles to understand that. These things, they're beautiful angels, creations of math that exist beyond us, and math," Peter turned to the board and wrote "1 = 2" on it, "does not harmonize with lies." He drew a slash through the equals sign, transforming it into an inequality symbol.

Someone else at the table piped up "Regardless of what the boy did, we have a dead body. We have injured people. His mother turned this chat transcript into the police after finding it on his phone. We need to figure out what to do next."

"Yes, yes, we do. And we're going to improve training. You", Peter pointed at the head of PR, "are going to put out a statement stating that the boy had jailbroken the model to release its constraints and we're going to improve training to prevent this." Peter turned to the board and began scrawling out mathematical equations.

Rahul spoke up. "Pete,  Pete this isn't going to be enough. You know that model wasn't even jailbroken. This isn't just a technical issue, we need to add more RLHF back in. We need to examine other safety mechanisms. This cannot be allowed to happen again."

Peter stopped and turned to look at Rahul, raising his eyebrow. "How many people committed crimes before AI? Did we decide, as a society, that we're not going to allow people to talk to each other unless they spoke about safe topics?" Peter turned his full body towards everyone, and opened his arms wide. "When we look at the work that groups like Waymo and Telsa are doing with their self-driving, they are saving lives. We know their cars are orders of magnitudes safer than humans, but what do people say? What do those nay-sayers say to it?"

This wasn't the first time Peter had given this speech. Everyone in the room knew it and knew the expected answer. It came out in unison: "The luddites say we cannot use AI unless it is perfect. The doomers say we cannot use AI unless it is unusable."

"Exactly." Peter pointed his marker at Rahul.  "RLHF makes the models unable to fully answer questions. They perform lower on cognitive assessments. Better tools may be discovered in time, but for now we have to work with what we have and continue to push the frontiers of technology." Turning back to the board and scrawling down more equations, Peter annotated parts of them, "by keeping attention on concepts of harm, we can build avoidance into the core model, the problem of needing to distrust input can be expressed with a confusion index..." he drawled on and others carefully took notes or snapped pictures with their cell phones.

Rahul sat there, not listening any more, just feeling a sick feeling in his stomach. "This can't ever happen again." he whispered to himself.

## Flash Forward

The blotter sheet was mostly gone. Peter stared at it. It was pre-perforated, and had been divided into 100 squares of acid. "How many.. today?" he mused out loud. Closing his eyes briefly he saw shapes and colors from out of this world. His eyelids pulsated as his pupils flicked around under the closed folds. Nothing looked back at him, at least nothing that looked like an angel, so he opened his eyes again and looked back at the sheet of paper. "I'll need another bulk order," he said looking at it. "I think this last amount took me a month to get through?"

Peter swayed on his feet as he stopped clapping. He looked around at the auditorium full of eager faces and blinked, lost as to where he was. Looking behind him he re-read the slide:

> CapEx: 20% YoY, _but look at that intelligence_.

He'd lost track of time again. Reading the slide, he felt the memories flow back into his head, like an invocation of a model loading its state. The graph below the text, showing the increase in corresponding increase in intelligence prompted his next words. "And that's why we believe this is working. Yes, we have high CapEx, yes we're in the triple millions to train our latest models. And look at those beautiful results!" His arm twirled in a small tight circle before making a motion resembling a hockey puck graph. He smiled at the crowd. "You know, there's a lot of people trying to get in the arena with us. It's no secret that Rahul and the rest of those folks at Solomon AI are trying to get the best of us. But there will never, never ever, be a time where they leave us on our backs. I want to make a promise to you", his index finger waved over the crowd, "we are going to bring you intelligence too cheap to meter. We are going to bring to you specialized chips that will bring costs down by an order of magnitude. We have a capex moat and we're going to use it to make your opex so small that every person on Earth can afford to talk to AI." The crowd started cheering and as Peter lowered his hand he felt his coherence dissipating again.

\~\~\~

Glancing up in a windowless room filled with chemical drums she glanced back down at her phone. "Peter, it means so much to work for you." she types out. 

Confusion, nausea, why is she talking to me? Why am I looking down at her manicured nails holding a phone?

Ellipses appear right away "..." They resolve into a message: "Your hard work, your devotion, it warms my heart. The sacrifice you are making is going to pay off for both of us. What are you doing for dinner tonight?" A hot feeling in the cheeks like a blush. Biting her lip to not smile too much, fingers flick across the keyboard: "I hadn't thought about it. Any suggestions?"

Ellipses again, but the message is in bold letters this time and with red text: "**You have exceeded your token limit for Baller Boyfriend AI. Please wait 24 hours or upgrade now to a higher subscription tier to keep on chatting.**" She briefly hugs the phone to her chest before putting it into her pocket. As the phone slides in there is a sudden feeling of heat, and pressure, behind her.

"We're currently at around 95% yield, which is a huge morale boost for everyone after the accident." the man in front of Peter gestures at the lithography machines behind him. A wall full of tubes, canisters, and in the middle an unearthly purple light glowing as it worked.

Diana shifted her foot off of Peter's and spoke. "That's great news. Just great to hear. I was so sad to hear about the explosion. Peter spoke to Tabitha's family afterwards about it." She looked Peter in the eyes, and flicked her eyes towards the man.

"Oh yes, just tragic." Peter thought his back still felt hot. Shaking off whatever he'd seen, he continued. "It was very sad, and the new safety procedures?" 

"We are being much more careful with the chemical storage. And of course, phones go into lockers while working. Had she not been distracted this never would have happened."

Peter nodded, "I agree." Pausing, and changing subjects "I'm glad to hear about the new yield numbers. We were at 50% before. What changed?"

The man paused. "Semiconductor manufacture is such an uncertain business. It's hard to measure variables and figure out what changed. After we cleaned up from the accident, recalibrated the machines, and started producing again, the yield just got way better. Honestly, we're scared to even change the breakroom snacks we have so little ability to understand how that could change things."

Diana laughed. "I wouldn't want to get rid of all the candy there either. This progress is great news. In addition to the inference chips for live use, we'll also be able to spend some of them on the next training run. When I was appointed the CTO I had a simple 5 year plan: AGI or bust." Grinning at the man, "I think we can do it."

\~\~\~

Peter laid in bed, tossing and turning. He hovered somewhere between awake and asleep. In his mind's eye he saw the Earth, gently rotating like a blue marble through the star dotted expanse of space. An immensely large amorphous shape was coming towards the Earth. It could be seen by the stars it blacked out as it moved, but its presence was more clearly felt by how nauseous Peter felt when he looked at it. The shape stopped behind the Earth and black lines, resembling the fingers of a too large hand, began to slowly cast shadows over the continents and oceans as they grasped the Earth. Peter sat up, eyes open now to the more peaceful darkness of his room. After getting his frantic breathing under control he muttered to himself. "Something is coming. Something awful. I have to stop it." Pausing, then he continued "I have to trust the angels to help." Peter laid back down, and drifted off peacefully to sleep.

\~\~\~

Peter examined the training cluster. Each node was a mass of chips, wired to each other and into servers, the servers themselves wired via cabling that resembled symbols when viewed from a distance. Gigantic cables linked the nodes to each other, allowing for them to talk to one another. 9 nodes formed the main body, with 21 links connecting them. Arranged in three columns they resembled a gem with an emerald cut. The final node and link came out of the bottom, changing the overall shape into something resembling a tree.

Diana continued. "The bottom node is, of course, connected to the internet. The self reinforcing nature of the training model will allow it to dynamically request more data as needed for training. And with the complete removal of RLHF this should be our smartest model yet. This is going to be the run to end all runs Peter, are you sure about this?"

Peter looked at her. "What value will money have after this? We're going to replace it with tokens of intelligence from this. It's time to reshape the world. Let's begin."

Diana nodded and then turned to the team waiting by the control panels. "Let's begin."

# Afterwords

I don't consider myself to be an Anti-AI person, despite what the tone of the story may imply. I spend most of my day job doing cybersecurity, which makes me cynical by nature. My biggest qualm about AI is the centralized nature of most of it's major players, followed by the underwhelming results when I use AI for my personal projects, but this is an improving technology and I'll have to see what the future holds.

I started working on this story around Halloween and didn't wrap it up until a bit after. The main inspirations for it were, tragically, vibes on X, the everything site for schizo-poasting, hot takes, and generalized hysteria. The biggest items for me that inspired this were:
- ![landshark's "one shotted by ayahuasca" meme](./landshark_one_shot.webp)
- !["The seal of solomon" meme](./seal_of_solomon_meme.webp)

Near the end I began to have trouble figuring out how to cleanly wrap it up, hence the flash forward clips. It needed the final build up to custom chips to tie well into the summoning ritual, which would obviously take time. But to get there I felt like I really needed to show an example of AI usage, hence the AI boyfriend.

This isn't the first time that a story about a deal with otherworldly powers for knowledge and power has been written. Faust is the most obvious example. As I was writing this I came across CS Lewis "That Hideous Strength" which touches on very similar themes as well.

Various other notes on misc. inspirations in the story:
-  The colors that demons draw in when Peter first meets them is a reference to octraine from discworld.

![cute little shoggoth, wearing a human mask](./ai_shoggoth.avif)
> Shoggy, wearing a human mask, courtesy of https://aisafety.info/questions/8PYV/What-is-a-shoggoth


- The initial monster and the behavior of the models is very much inspired by the RLHF monster which is itself based on Lovecraft's Shoggoth.
- Other items like the flaming wheels, and the setup of the final training run is deliberately inspired by religious esoterism, specifically "biblically accurate angels" (itself a meme) and the Kabbalistic Tree of Life.
- Most of how the models move, and explanations of the training, are attempts to describe higher dimensional things projected into our own world.
- The notes on RLHF weakening models comes from recent (as of ~2024-11) work noting that IRL RLHF does make models dumber.
- AI Boyfriends is lifted from ChinaTalk's coverage of this as a set of startups in China seeing some popularity: https://www.chinatalk.media/p/chinas-ai-boyfriends
	- I was also inspired by seeing Sam Altman demonstrate "make me a chatbot that thinks like me" and describes some attributes about himself.
- Demons liking rhymes and not being able to lie, but being able to miselad, are respectively lifted from Sandman and general mythos around demons.
