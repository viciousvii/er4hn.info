---
title: "(Suggested 📚) Help - I Have a Manager!"
description: "Then Life Continued - Suggested Reading"
summary: Review of "Help - I Have a Manager" by Julia Evans.
tags: ["Then Life Continued", "Suggested Reading"]
date: 2022-08-28
---

## Deets
- Help! I have a Manager by Julia Evans
- Available at https://wizardzines.com/zines/manager/

## Review
Help! I have a Manager! is a Zine by Julia Evans. Julia typically designs short, illustrated, guides to computer science concepts like Linux tools, how containers work, DNS, and other similar subjects. This zine has her branching out to covering how to work effectively with your manager. It is  a zine on soft skills.

I was drawn to this guide because these conversations are a weakness of mine. I was never sure what to talk about with my manager and when I began to oversee others I likewise struggled with what to do in a one on one. The importance of holding those meetings was clear, how else can you check in with someone, but what to do in them was less clear to me. I wanted to avoid awkward silences and a problem I've heard referred to as "big eyes looking at little eyes".

This zine was very helpful at making me feel more effective in these conversations. It sets up what to expect from your manager and how to work with them to achieve your own goals by using positive examples. There are lots of suggestions about topics to discuss, areas to focus on, and why your manager would be interested in particular topics.

By making use of this guide I was able to avoid silences and understand each of my team members goals. That in turn opened up opportunities to help them improve and understand their current morale. I would suggest that anyone, manager and non-manager, get a copy of this for their own use.
