---
title: "(Suggested 📚) slide:ology"
description: "Then Life Continued - Suggested Reading"
summary: Review of "slide:ology" by Nancy Duarte.
tags: ["Then Life Continued", "Suggested Reading"]
date: 2022-08-28
---

## Deets
- slide:ology by Nancy Duarte
- ISBN: 978-0596522346

## Review
slide:ology is one of the frequently referenced books on how to build, not great slides, but great presentations. A common point of confusion is that slides are the important bit and that a meeting is just an effort to read off slides emailed in advance or after. This is not true and is a myth slide:ology works to dispel.

Slides are the visual elements that give a presentation more impact than oration. Slides provide guidance to the current talking points, they share images that evoke emotions. Slides can hint towards where the presentation is going next or illustrate how the different points are connected.

slide:ology provides a general overview for how to arrange information in slides. Discussed inside is how to plan out what to put in a presentation, and therefore a slide. Displaying data, tips on coherent design, and the few places animations are acceptable are all covered inside.

My overall feeling on slide:ology is that it is very valuable, but mostly at the start. The first few chapters discuss very useful information on how to build and arrange slides. Highlighting key data, avoiding information overload, and other key tips are very useful.

As the book goes on, around the halfway point, I see the utility of its contents begin to fall off for me. Duarte is a professional with artistic abilities and a design team. Much of her advice around things like proper use of animation or custom 3D designs feel like concerns that I would not have in my normal presentation building. Some of the content even begins to feel like it is there to pad the page count.

The first half alone is well worth the price of purchase however. I would recommend getting this book as a physical copy since it is very visual and worth flipping through different sections as you are building your own slides.
