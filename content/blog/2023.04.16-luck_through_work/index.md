---
title: "(🔥 Take) Luck Through Work"
description: "Then Life Continued - Hot Takes"
summary: How to achieve better outcomes through work
tags: ["Then Life Continued", "Hot Takes"]
date: 2023-04-16
---

This is a hot take on Jason Roberts blog post: https://www.codusoperandi.com/posts/increasing-your-luck-surface-area

"Luck" is the term used to quantify and measure the outcome of events beyond ones control. Winning the lottery: good luck. Tripping when running and breaking ones leg: bad luck. Luck is typically spoken about like it is something you possess: "I have bad luck", "I got lucky that day". Superstitious attempts are traditionally made to increase ones luck: A rabbits foot carried around, a horseshoe hung inside the house. Yet the cause and effect between tailsmans and events beyond control cannot be observed. Luck, slippery as it may be, can be increased via carrying out another set of rituals.

To begin, one has to view luck as a space on a 2d plane.

![Luck as a plane](./Luck_Plane.png)
> Drawing of a flat plane, with a yellow circle representing ones luck. Events happen outside the plane, pass through, but never intersect the circle.

Rays, events, fall from outside the plane and pass through it. If a ray intersects with your luck, however it is shaped, that event happens to you. You get a free scoop of guacamole with your burrito, the lights are green your entire commute to work. Luck no longer has bad things under this definition, these are just the positive events you want to happen. But this is still too broad of a definition for luck. The things you can do to increase your luck aren't magic 🔮 The lucky events you can try and have happen more often aren't around fortuitous physical timing, they're around finding chance moments of human connection.

Many of the lucky breaks that happen in ones life are the result of human connection. Getting the job that you really want, finding an investor for a project, getting that free scoop of guacamole from a friendly restaurant worker, they all involve moments of meeting people and have those people be willing to help you. Having these moments happen to you are something you can control for and try to have happen more frequently. The actions that control the shape of your luck are what you do and how you tell others about that.

To create things is to be doing. Building some new database optimization tool, crafting a better mousetrap, recording yourself going through your favorite hiking trail. These are all acts of creativity where you bring something into the world that wasn't there before. Creation can take many forms and even something like reviewing other peoples products is a form of creation. What you are creating is bringing your own thoughts and experiences to someone else's product. This is what a good deal of the influencer industry, for better or worse, is built upon.

Creation is one axis of luck. The other axis is to tell people about what you are doing. You must share your creations, promote them at events, shout about what you're doing from the rooftops and across the internet. This is because on their own these creations will not be discovered. There are billions of people making hundreds of millions of pieces of their own content. Without telling other people about what you are doing, they will never know.

![Luck as a plane v2](Luck_Plane_Hit.png)
> Another drawing of luck as a plane. Now this time a ray of human connection has hit the person's luck.

Now the theory begins to come together. Human beings do not exist in a vacuum. We are pack animals and by working together we are able to achieve things that could never be done alone. The things that a person would desire: Fame, fortune, improving lives around them, are not things done on their own. They need to be shown to people until someone is found who feels the same way as the author about this new thing that has been created. Then, as the author, you've gained a supporter. This supporter can help you in ways you could do not on your own: They can help promote your work, they can provide you funding, they can help you improve upon your original creation. 

![Do Tell Chart](DoTellChart.png)
> Graph showing how narrow it is to focus on a single axis, and how by focusing on both, more can be achieved.

It is hard work to do both, but it's essential. Focusing on one without the other limits how far you can go. One can only tell others about something so many times before they'll want to see it. Likewise when one builds in complete isolation, it is hard to say if the thing being built actually solves a problem or is the optimal way to do so. But when both are combined one can create, receive feedback, create more, find supporters, and build further upon their support. It's difficult work to balance, but for those that can the odds of success are vastly improved.
