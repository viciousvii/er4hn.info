---
title: "(Suggested 📚) The Real Life MBA"
description: "Then Life Continued - Suggested Reading"
summary: Review of "The Real Life MBA" by Jack and Suzy Welch.
tags: ["Then Life Continued", "Suggested Reading"]
date: 2022-08-28
---

## Deets
- The Real Life MBA by Jack and Suzy Welch
- ISBN: 978-0062362803

## Review

One of the inspirations for this blog came from picking up and reading The Real Life MBA years ago. I was young, I was having trouble managing the transition from being an engineer on a team to being a leader on teams. I felt inadequate without having been trained on the "business" and "soft" skills I had not considered important. The Real Life MBA gave me confidence that I could do well in my job without needing a formal degree to do so.

The book overall is fairly simplistic. The advice is not deep and historians may notice that much of what is said does not match what Jack Welch did. Jack Welch will be a controversial figure for a long time to come, but his legacy as a person is not the subject of this review. Where this book excels is in providing a set of basic principles for being a useful leader. As long as you don't expect deep stories based on experiences, your expectations should be properly calibrated.

### Takeaways
These are my notes from reading the book. They are provided to help others decide if the full book is worth reading:

* Build your brand:
	* Be known as a good person to everyone. 
	* Don't be unscrupulous since that will cause others to react poorly to you.
* Have a social media presence that displays who you are as well. 
* Having a clear mission is important. This gives others something to align themselves around.  
* The best thing a manager can do is remove obstacles.
* When dealing with someone you don't understand, make it clear you don't understand and dig into the details until you do.
* Find what you are most effective at and passionate about. This is your "Area of Destiny" and leaning into it is where you will be the most effective.
	* Principles by Ray Dalio mentions this as well. One ancillary point that Dalio makes is that you should surround yourself with people who are strong in places you are not. This leads to you having a more effective team.
