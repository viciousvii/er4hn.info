---
title: "Times the Torment Nexus has been created"
description: "Then Life Continued - my personal blog"
summary: The torment nexus as a repo of tech ideas.
tags: ["Then Life Continued"]
date: 2022-08-18
---

![Torment Nexus Twitter pic](./tormentnexus.jpg)
> Torment Nexus, defined. Taken from https://twitter.com/alexblechman/status/1457842724128833538?lang=en

**Please write to me with times you have seen a Torment Nexus be created**:

(This is all a set of spoilers for books and movies. Some spoilers may be a somewhat sketchy take on the novel for the sake of fitting into my narrative)

- Metaverse: From Neal Stephensen's "Snow Crash" the Metaverse was a virtual reality world that let the poor, and outcasts, of society express themselves online in ways they could never afford to in the real world. This was exploited by an evil billionaire who used memes to attempt to control the world. Sometime during reading the book Mark Zuckerburg would decide "this is the future" and rename Facebook to Meta.
- Surveillance Tech: JRR Tolkein's Lord of the Rings featured a device known as the "Palantir". This was a crystal ball that would show the user whatever they wanted to see in the world. It was possible for Sauron to get the Palantir stones to show true, but misleading, images to track users of them. Peter Thiel would think "this is perfect" as he named his surveillance analytics company after it and sold it to police and military for use in managing civilian populations.
- Artificial Intelligence: The Terminator series of movies featured an antagonistic artificial intelligence known as "SKYNET". After gaining sentience SKYNET decides to destroy humanity and causes a series of devastating wars. "I like the war and destruction bit" said someone at the NSA as they chose that name for their automated terrorist identification program. 
- Panopticon: Jeremy Bentham, the founder of utilitarianism, came up with the concept of a "[Panopticon](https://en.wikipedia.org/wiki/Panopticon)" in 1786. The concept was a prison where prisoners would not know if they were being observed, and would have to behave as though they were under constant observation. This was designed with the best of intentions and nuanced guardrails were a part of the design to ensure humane treatment of the prisoners and accountability of the guards. Subsequent implementations of the panopticon would cause Jeremy Bentham to coin the phrase "sinister interest" when describing how "the vested interests of the powerful would conspire against a wider public interest". Amazon would sell devices such as Ring and create grocery stores like Amazon Fresh. The former would become a clear example of sinister intent after Ring recordings were repeatedly turned over to police without a warrant. The latter enabled a hedonistic grocery store experience, where you could eat items before even hitting the checkout stand, confident in the knowledge that you were being watched and billed appropriately.
