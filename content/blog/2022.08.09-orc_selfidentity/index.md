---
title: "I am an Orc"
description: "Then Life Continued - my personal blog"
summary: I identify as an Orc
tags: ["Then Life Continued"]
date: 2022-08-09
---

The Lord of the Rings is one of the most influential and popular fantasy novels of all time. In nerd and geek culture LOTR is a cultural reference just about everyone will understand. References to lord of the rings abound in the tech industry as well. Peter Thiel named his surveilence company "Palantir" after the all seeing orb from the novels. The Salesforce tower in San Francisco celebrates Halloween by displaying the "Eye of Sauron" from it's top. Google Maps, at one point, had a little easter egg where it would not display walking directions from "The Shire" to "Mordor".

The irony of how much people in the tech industry identify with Lord of the Rings is that many of us are orcs. The degraded, dirty, monsters and foot soldiers of the series antagonists. Perhaps not everyone in the industry, but I identify myself as an orc.

![Some orcs](./orc.jpg)
> Myself and some others trying something new

LOTR is a novel which shows a world that has slowly lost its glory over time. The greatest of times were in the past ages. Elves once ruled as the dominant species, magic was more prominent, and great artifacts were created by the most talented of prior ages. As time went on the world gradually got worse. Melkor, the Lucifer of Middle-Earth, conspired to destroy artifacts such as the Two Trees. He also waged destructive wars and personally slew heros.

By the time of the events in the novel, the world is in what is referred to as the "Third Age". Magic is gradually fading away from the world. Man, a short lived and corruptible species, is dominant. Even the threats that are faced are shadows of their former glory. Sauron was the lieutenant of Melkor. Sauron, the antagonist of the series, barely exists on the world as a broken body relaying orders from his fortress. Smaug, the dragon of The Hobbit, is the last of it's kind and size. And even Smaug was a small drake compared to it's parents of legend.

Then come the Orcs. Corrupted elves and men twisted by Melkor. The Orcs had lost the beauty of the natural world and their bodies, but they were clever and crafty. They did not make beautiful things, but they made machines with wheels, engines, and tools. In Isengard the Orcs built factories and furnaces to craft and churn out parts for their war effort. The Orcs often functioned as foot soldiers, violent and incohesive, pressed into service, but they shared something in their nature with the people of today.

The Orcs were able to survive in harsh conditions that tested the abilities of the Free Elves, men, and dwarves of Middle-Earth. In the plains around Mordor the Orcs used volcanic ash to grow food. They built machines and factories to overcome their problems. The orcs did not wait around for ancient prophecies to defeat their foes nor search for their arms and armor in old caves that their ancestors lived in. 

Tolkein wrote about the Orcs, Isengard, and his view of the world as a reaction to the circumstances of his own life. He lived in the countryside and greatly enjoyed the pastoral life. He served in the first World War and saw firsthand the horrors wrought by new machines of war. It is very likely that the pollution from English factories blackened the skies where he lived as time went on. Tolken had a great deal of reverence for the past and history as well. It shows through in his detailed weaving of mythological creatures, Christianity, and all the other elements he put into Middle-Earth. In his own life he was someone who refused to speak anything but Latin in church, long after others had switched to English. It was natural that the Orcs would be monsters and enemies in his stories.

Yet, in the Orcs, there is also something to be admired. They were builders and creators in a world that demanded accepting the status quo. With their factories and tools the Orcs bent the land and matter to their will. Magic was not used by them to perform their works, but intelligence and cunning and understanding of the systems of the world. When the rest of the world looked backwards the Orcs looked forwards.

The tech industry is one which is also built on the overturning of the prior order. To be successful and celebrated in tech, you have to launch a new startup and disrupt the prior order. If you can craft something which is 10x better than what was before, people will use your product instead of the old thing. Tech constantly pushes forwards the limits of what is possible. Artifical intelligence based image generation, microchips, always-open online stores, games, cars with electric engines, even the original combustion engine car were disruptors of what was there before. 

The sterotype of the 90s was that a software engineer is going to look like an Orc as well. Diminutive, shy, but crafty, was the popular description of a SWE. It's no longer as true, but tech is not a field that naturally attracts the backslappers and lifes of the party.

The negative externatilies Tolkein hated are also unfortunately true within my industry. The rise of tech lead to the creation of more and more pieces of disposable junk. Intel contributed to Superfund sites throughout the South Bay Area with their disposal of toxic chemicals down the sink, until the melting of sink pipes indicated they had a problem. Companies such as Meta, Uber, PayPal, Amazon, and Google distorted markets, behaved irresponsibly with data, and harmed the occassional person. Often this was not intentional, but a side effect of the dual mentalities of "growth at all costs" and "move fast, break things" that they espoused as they operated.

I'm not the only person to have come to this conclusion. "The Last Ringbearer" covers very similar themes, though presented as a story. I also want to stress that I do not hate the world of Middle-Earth. The Hobbit was one of my favorite childhood tales. There is something wonderful about a wizard showing up at the house of a homebody and telling him "come on loser, we're going to rob a dragon".
