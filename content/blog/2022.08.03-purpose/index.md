---
title: "Statement of Purpose"
description: "Then Life Continued - my personal blog"
summary: Why am I doing this?
tags: ["Then Life Continued"]
date: 2022-08-03
---

When starting on something, it is important to have a vision of what you want to accomplish. The vision describes _why_ you are doing this. Vision is not concerned with the implementation details. The next level down from vision is the mission. Mission is what you intend to achieve. In the mission you must describe what is in the scope of the mission and what is out of scope. Next, below mission, is strategy. Strategy is how to achieve the mission. Strategy involves looking around at the state of the world and deciding, based on data and analysis, what to concretely do.

With that framework laid out, why am I starting a blog? I am doing it because I believe that my thoughts have some value. The best value comes from having thoughts which cross the divide from being wisps of vapor to having palpable form. Those two sentences are my vision. If I really wanted to pare it down, I could just keep the first sentence.

The mission to achieve my vision is to force myself to give form to my thoughts. I want to elaborate on ideas that persistently bounce around my head and lay them out in a way which dives into the details of them and makes them feel real and meaningful. If I describe a new technology or a paradigm of engineering, it should be possible to build off of it. When describing a book, the value proposition of reading the book and what I got from it should be clear. I do not want to use this blog to shitpost, that is much more fun on social media where people can reply and have emoji reactions. I don't want to use this blog to journal the minuate, anxieties, and ennui of my life, I have a private journal for that. Finally, I don't want to use this blog to share short snippets of thought. What I write here should have some substance.

The final item to follow through on this is: why the blog? Why not use Twitter, Facebook, TikTok videos set to music that imparts an atmosphere of profundity? The answer to that, the strategy in using a blog format, is that I want to own the content and have it exist in the form which I choose. Placing this content on social media platforms encourages me to use the platform in the way that the platform is structured and gatekeeps who can see it. Neither of those qualities are, at present, of any interest to me.

To conclude, I am excited to see if my statements have any purpose.
