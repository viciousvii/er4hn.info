{
  description = "er4hn.info blog";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.05";

    # Import theme
    # Most of this flake, including this, lifted from https://averyan.ru/en/p/nix-flakes-hugo/
    papermod = {
	url = github:adityatelange/hugo-PaperMod/0989c28a0ecdf674906a18d663a4f806b49ec03f;
	flake = false;
    };
  };

  outputs = { self, nixpkgs, papermod }:
    let

      # to work with older version of flakes
      lastModifiedDate = self.lastModifiedDate or self.lastModified or "19700101";

      # Generate a user-friendly version number.
      version = builtins.substring 0 8 lastModifiedDate;

      # System types to support.
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];

      # Helper function to generate an attrset '{ x86_64-linux = f "x86_64-linux"; ... }'.
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

      # Nixpkgs instantiated for supported system types.
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });

    in
    {

      # Provide some binary packages for selected system types.
      packages = forAllSystems (system:
        let
          pkgs = nixpkgsFor.${system};
        in
        {
	blog = pkgs.stdenv.mkDerivation {
	name = "blog";
	inherit version;
# Exclude themes and public folder from build sources
	src = builtins.filterSource
	(path: type: !(type == "directory" &&
		       (baseNameOf path == "themes" ||
			baseNameOf path == "public")))
	./.;
# Link theme to themes folder and build
	buildPhase = ''
	mkdir -p themes
	ln -s ${papermod} themes/PaperMod
	${pkgs.hugo}/bin/hugo --minify
	'';
	installPhase = ''
	cp -r public $out
	'';
	meta = with pkgs.lib; {
		description = "er4hn.info - blog";
		platforms = platforms.all;
	};
	};
        });
      
      # Add dependencies that are only needed for development
      devShells = forAllSystems (system:
        let 
          pkgs = nixpkgsFor.${system};
        in
        {
          default = pkgs.mkShell {
            buildInputs = with pkgs; [ hugo ];
	    # Link theme to themes folder
            shellHook = ''
	    mkdir -p themes
	    ln -sf ${papermod} themes/PaperMod
	    echo "Hello Shell!"
          '';
          };
        });

      # The default package for 'nix build'. This makes sense if the
      # flake provides only one package or there is a clear "main"
      # package.
      defaultPackage = forAllSystems (system: self.packages.${system}.blog);
    };
}
